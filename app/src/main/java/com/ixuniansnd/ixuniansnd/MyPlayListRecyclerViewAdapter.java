package com.ixuniansnd.ixuniansnd;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import com.ixuniansnd.ixuniansnd.ictAudioVisualizer;

import com.ixuniansnd.ixuniansnd.ictMediaFragmentBase.OnListFragmentInteractionListener;
import com.ixuniansnd.ixuniansnd.ictSpinnerReselect;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.RunnableFuture;

import static com.ixuniansnd.ixuniansnd.MyPlayListRecyclerViewAdapter.MyPlaylistType.ICT_PLAYLIST;
import static com.ixuniansnd.ixuniansnd.MyPlayListRecyclerViewAdapter.MyPlaylistType.ICT_SONG_LIST;


/**
 * {@link RecyclerView.Adapter} that can display  and makes a call to the
 * specified {@link OnListFragmentInteractionListener}.
 * TODO: Replace the implementation with code for your data type.
 */
public class MyPlayListRecyclerViewAdapter extends RecyclerView.Adapter<MyPlayListRecyclerViewAdapter.ViewHolder> implements ictItemTouchHelperCallback.ictItemTouchHelperAdapter {

    enum MyPlaylistType
    {
        ICT_SONG_LIST,
        ICT_PLAYLIST
    }
    private List<ictFileInfo> mValues;
    private final OnListFragmentInteractionListener mListener;
    private Context m_Context = null;
    int m_dwPosition = 0;
    MyPlaylistType m_ePlaylistType = ICT_SONG_LIST;


    public MyPlayListRecyclerViewAdapter(MyPlaylistType a_eType, Context a_Context, List<ictFileInfo> items, OnListFragmentInteractionListener listener) {
        mValues = items;
        mListener = listener;
        m_Context = a_Context;
        m_ePlaylistType = a_eType;
    }

    void reInit( List<ictFileInfo> a_FileInfo )
    {
        mValues = a_FileInfo;
        notifyDataSetChanged();
    }

    public boolean onItemMove(int a_dwFromPos, int a_dwToPos )
    {
        if(((MainActivity)m_Context).MoveItemInPlayList(a_dwFromPos, a_dwToPos)) {

            List<ictFileInfo> pCurrPlaylist = ((MainActivity)m_Context).getPlaylist();
            notifyItemMoved(a_dwFromPos, a_dwToPos);

            mValues.clear();
            int dwSongIndex = ((MainActivity) m_Context).GetSongPos();

            int songPos = 0;
            int dwCounter = 0;
            for( ictFileInfo media : pCurrPlaylist) {
                if(media.getIndex() == dwSongIndex) {
                    songPos = dwCounter;
                }
                mValues.add(media);
                dwCounter++;
            }

            mListener.SendSongList(pCurrPlaylist, songPos);
            return true;
        }
        return false;
    }

    public void onItemDismiss( int a_dwPos )
    {
        if(((MainActivity)m_Context).DeleteSongFromCurrPlaylist(mValues.get(a_dwPos)))
        {
            //CoordinatorLayout vView = (CoordinatorLayout)((MainActivity)m_Context).findViewById(R.id.main_content);
            ictPlayListView playListView = ((MainActivity)m_Context).GetPlaylistViewFrag();
            if ( playListView != null) {
                playListView.hideController();

                RecyclerView vView = (RecyclerView)playListView.getView().findViewById(R.id.playlist_view);
                Snackbar undo = Snackbar.make(vView, R.string.song_removed, Snackbar.LENGTH_LONG);
                undo.setAction(R.string.undo, new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {

                                ((MainActivity)m_Context).UndoLastDeleted();
                            }
                        }
                ).setActionTextColor(Color.WHITE);

                undo.getView().setBackgroundColor(Color.GRAY);
                undo.show();
            }

            mValues.remove(a_dwPos);
            if(mListener != null) {
                mListener.DeleteMedia( a_dwPos );
            }
            notifyItemRemoved(a_dwPos);
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_playlist, parent, false);
        return new ViewHolder(view);
    }

    protected void SetIconView( final ViewHolder holder, int position )
    {
        ictFileInfo info = mValues.get(position);
        int dwPreSelectedIdx = -1;
        if( m_Context instanceof MainActivity)
        {
            dwPreSelectedIdx = ((MainActivity)m_Context).GetSongPos();
        }
        if(mValues.get(position).getIndex() == dwPreSelectedIdx)
        {
            holder.mIdView.setVisibility( View.GONE);
            holder.m_Visualizer.setVisibility( View.VISIBLE );
        }
        else
        {
            holder.mIdView.setVisibility( View.VISIBLE);
            holder.m_Visualizer.setVisibility( View.GONE );
        }
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        holder.mContentView.setText(mValues.get(position).GetFileName());
        holder.m_dwFileInfoIndex = mValues.get(position).getIndex();

        SetIconView ( holder, position );

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    setPositionIdx(position);
                    // Notify the active callbacks interface (the activity, if the
                    // fragment is attached to one) that an item has been selected.
                    mListener.SendSongList(mValues, /*mValues.get(position).getIndex()*/position);
                    mListener.onListFragmentInteraction(mValues.get(position), position);
                    notifyDataSetChanged();
                }
            }
        });

        holder.m_MenuContext.setOnLongClickListener( new View.OnLongClickListener()
        {
            @Override
            public boolean onLongClick(View view)
            {
                setPositionIdx(position);
                return false;
            }
        });

    }

    void setPositionIdx ( int a_dwPosition )
    {
        m_dwPosition = a_dwPosition;
    }

    int getPositionIdx() { return m_dwPosition; }

    @Override
    public int getItemCount() {
        if(mValues == null )
            return 0;
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnCreateContextMenuListener {
        public final View mView;
        public final ImageView mIdView;
        public final TextView mContentView;
        public ictAudioVisualizer m_Visualizer = null;
        public final ImageView m_MenuContext;

        public int m_dwFileInfoIndex = -1;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            mIdView = (ImageView) view.findViewById(R.id.media_type);
            mIdView.setImageResource(R.drawable.ic_music_note);
            mContentView = (TextView) view.findViewById(R.id.content);
            m_Visualizer = (ictAudioVisualizer)view.findViewById(R.id.graphic_vis);
            m_MenuContext = (ImageView)view.findViewById(R.id.playlist_ctx_img);
            m_MenuContext.setImageResource(R.drawable.ic_more_vert_black_24dp);
            m_MenuContext.setOnCreateContextMenuListener(this);
        }

        @Override
        public void onCreateContextMenu(ContextMenu menu, View view, ContextMenu.ContextMenuInfo info)
        {
            if(m_ePlaylistType == ICT_SONG_LIST) {
                MenuItem playListItem = menu.add(Menu.NONE, R.id.playlist_ctx_img, Menu.NONE, R.string.playlist_label);
                playListItem.setOnMenuItemClickListener(onPlayListContextMenu);
            }
            else if ( m_ePlaylistType == ICT_PLAYLIST)
            {
                MenuItem playListItem = menu.add(Menu.NONE, R.id.playlist_ctx_img, Menu.NONE, R.string.delete_song);
                playListItem.setOnMenuItemClickListener(onPlayListContextMenu);
            }
        }

        private abstract class SpinnerRunnable implements Runnable
        {
            boolean m_bFirst = false;
            SpinnerRunnable( boolean a_bFirst)
            {
                m_bFirst = a_bFirst;
            }
        }

        private void ShowPlaylistOpDialog()
        {
  /*
            PopupMenu pPopup = new PopupMenu(m_Context, this.itemView);
            MenuInflater inflater = pPopup.getMenuInflater();
            inflater.inflate(R.menu.playlist_op_menu, pPopup.getMenu());
            pPopup.show();
   */
            // 1. Instantiate an AlertDialog.Builder with its constructor
            final AlertDialog.Builder builder = new AlertDialog.Builder(m_Context);
            builder.setTitle(R.string.playlist_label);
            LayoutInflater playListSelInfl = ((MainActivity) m_Context).getLayoutInflater();

            final View playListSelView = playListSelInfl.inflate(R.layout.playlist_sel, null);
            builder.setView(playListSelView);

            builder.setPositiveButton(R.string.new_playlist, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            final AlertDialog.Builder pNewPlaylist = new AlertDialog.Builder(m_Context);
                            pNewPlaylist.setTitle(R.string.new_playlist);
                            LayoutInflater layoutInfl = ((MainActivity) m_Context).getLayoutInflater();
                            View promptNewPlaylistView = layoutInfl.inflate(R.layout.create_playlist, null);
                            pNewPlaylist.setView(promptNewPlaylistView);

                            final EditText playListCreateName = (EditText) promptNewPlaylistView.findViewById(R.id.playlist_name);

                            pNewPlaylist.setPositiveButton(R.string.create_playlist, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    String sNewPlaylist = playListCreateName.getText().toString();
                                    if (m_Context instanceof MainActivity) {
                                        int posCursor = getPositionIdx();
                                        if (posCursor == RecyclerView.NO_POSITION) {
                                            Log.e("IANE_", "Invalid song position");
                                        }
                                        else {
                                            ((MainActivity) m_Context).CreatNewPlaylist(sNewPlaylist, sNewPlaylist, mValues.get(posCursor));
                                        }
                                    }
                                }
                            });
                            pNewPlaylist.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialogInterface1, int i) {

                                }
                            });
                            AlertDialog pCreateDiag = pNewPlaylist.create();
                            pCreateDiag.show();
                        }
                    }
            );
            builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {

                }
            });

            builder.setNeutralButton(R.string.ok, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    ictSpinnerReselect pSpinner = (ictSpinnerReselect) playListSelView.findViewById(R.id.playlist_dropdown);
                    if (pSpinner != null) {

                        Object obj = pSpinner.getSelectedItem();
                        if( obj instanceof String) {
                            //int dwIndex = pSpinner.getSelectedItemPosition();

                            String playlistName = (String) obj;
                            String fileName = mValues.get(getAdapterPosition()).GetFileName();

                            ((MainActivity) m_Context).AddToPlaylist(playlistName, mValues.get(getAdapterPosition()));
                        }
                    }
                }
            });

            final AlertDialog dialog = builder.create();
            final ictSpinnerReselect pSpinner = (ictSpinnerReselect) playListSelView.findViewById(R.id.playlist_dropdown);
            if (pSpinner != null) {
                List<String> pDescriptionProxy = new ArrayList<>();
                ((MainActivity) m_Context).GetPlaylistEntries(pDescriptionProxy);
                List<String> pDescription = new ArrayList<>();
                int dwPlaylistIndex = 0;
                int dwPlaylistIndexFnd = -1;
                String m_sLastPLaylist = ((MainActivity)m_Context).GetPlaylistLastSelected();
                for (String str : pDescriptionProxy) {
                    if( str == m_sLastPLaylist)
                    {
                        dwPlaylistIndexFnd = dwPlaylistIndex;
                    }
                    dwPlaylistIndex++;

                    pDescription.add(str);
                }
                ArrayAdapter<String> dataAdapter = new ArrayAdapter<>(m_Context,
                        android.R.layout.simple_spinner_item, pDescription
                );
                dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                pSpinner.setAdapter(dataAdapter);
                if(dwPlaylistIndexFnd >= 0)
                {
                    pSpinner.setSelection(dwPlaylistIndexFnd);
                }
                pSpinner.post(new Runnable() {
                    public void run() {
                        pSpinner.setOnItemSelectedEvenIfUnchangedListener/*pSpinner.setOnItemSelectedListener*/(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                                String playlistName = adapterView.getItemAtPosition(i).toString();
                                String fileName = mValues.get(getAdapterPosition()).GetFileName();
                                Log.i("IAN_", "Sel1: " + playlistName + " fileName=" + fileName);
                                ((MainActivity) m_Context).AddToPlaylist(playlistName, mValues.get(getAdapterPosition()));
                                dialog.cancel();
                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> adapterView) {
                            }
                        });
                    }

                });
            }
            /**
            final Spinner pSpinner = (Spinner) playListSelView.findViewById(R.id.playlist_dropdown);
            if (pSpinner != null) {
                List<String> pDescriptionProxy = new ArrayList<String>();
                ((MainActivity) m_Context).GetPlaylistEntries(pDescriptionProxy);
                List<String> pDescription = new ArrayList<String>();
                pDescription.add(new String(""));
                for (String str : pDescriptionProxy)
                    pDescription.add(str);

                ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(m_Context,
                        android.R.layout.simple_spinner_item, pDescription
                );
                dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                pSpinner.setAdapter(dataAdapter);
                pSpinner.post(new SpinnerRunnable(true) {
                    public void run() {
                        pSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                                if (!m_bFirst) {

                                    String playlistName = adapterView.getItemAtPosition(i).toString();
                                    String fileName = mValues.get(getAdapterPosition()).GetFileName();
                                    Log.i("IAN_", "Sel1: " + playlistName + " fileName=" + fileName);

                                    ((MainActivity) m_Context).AddToPlaylist(playlistName, fileName);
                                    dialog.cancel();
                                }
                                m_bFirst = false;
                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> adapterView) {
                            }
                        });
                    }

                });
            }*/
            dialog.show();
        }

        private final MenuItem.OnMenuItemClickListener onPlayListContextMenu = new MenuItem.OnMenuItemClickListener() {

            @Override
            public boolean onMenuItemClick(MenuItem item) {
                    switch (item.getItemId()) {
                        case R.id.playlist_ctx_img: {
                            if(m_ePlaylistType == ICT_SONG_LIST ){
                                ShowPlaylistOpDialog();
                            }
                            else if(m_ePlaylistType == ICT_PLAYLIST) {
                                int posCursor = getAdapterPosition();
                                if (posCursor != -1) {
                                    String songName = mValues.get(getAdapterPosition()).GetFileName();
                                    if(((MainActivity)m_Context).DeleteSongFromCurrPlaylist(mValues.get(getAdapterPosition()))) {
                                        int pos = getAdapterPosition();
                                        mValues.remove(pos);
                                        mListener.DeleteMedia(pos);
                                        notifyItemRemoved(pos);
                                        notifyItemRangeChanged(pos, mValues.size());
                                    }
                                }
                            }
                        }
                            break;
                    }
                    return true;
                }
        };


        @Override
        public String toString() {
            return super.toString() + " '" + mContentView.getText() + "'";
        }
    }
}
