package com.ixuniansnd.ixuniansnd;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.os.Build;
import android.speech.RecognizerIntent;
import android.speech.SpeechRecognizer;
import android.speech.RecognitionListener;
import android.util.Log;
import android.os.Bundle;
import android.os.Handler;

import java.util.ArrayList;


/**
 * Created by trong on 5/3/17.
 */

public class ictSpeechModule {
    private final String TAG = "IAN_";
    protected AudioManager m_AudioManager = null;
    protected SpeechRecognizer m_SpeechRecognizer = null;
    protected Intent m_SpeechRecognizerIntent = null;
    protected boolean m_bIsListening = false;
    private boolean m_bIsStreamSolo = false;

    protected OnSpeechResultsListener m_SpeechResults = null;

    public boolean IsListening()  { return m_bIsListening; }

    ictSpeechModule (Context a_Context, OnSpeechResultsListener a_OnResults )
    {
        try
        {
            m_SpeechResults = (OnSpeechResultsListener)a_Context;
        }catch(ClassCastException e)
        {
            Log.e(TAG, e.toString());
        }
        m_AudioManager = (AudioManager)a_Context.getSystemService(Context.AUDIO_SERVICE);
        m_SpeechRecognizer = SpeechRecognizer.createSpeechRecognizer(a_Context);
        m_SpeechRecognizer.setRecognitionListener( new SpeechRecognitionListener() );
        m_SpeechRecognizerIntent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        m_SpeechRecognizerIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        m_SpeechRecognizerIntent.putExtra(RecognizerIntent.EXTRA_CALLING_PACKAGE, a_Context.getPackageName());
        startListening();
    }

    public void startListening()
    {
        if(!m_bIsListening)
        {
            m_bIsListening = true;
            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN)
            {
                if(!m_bIsStreamSolo)
                {
                    m_bIsStreamSolo = true;
                }
            }
            m_SpeechRecognizer.startListening(m_SpeechRecognizerIntent);
        }
    }

    public void destroy()
    {
        m_bIsListening = false;
        if(!m_bIsStreamSolo)
        {
            m_bIsStreamSolo = true;
        }
        if(m_SpeechRecognizer != null)
        {
            m_SpeechRecognizer.stopListening();
            m_SpeechRecognizer.cancel();
            m_SpeechRecognizer.destroy();
            m_SpeechRecognizer = null;
        }
    }

    private void listenAgain()
    {
        if(m_bIsListening)
        {
            m_bIsListening = false;
            m_SpeechRecognizer.cancel();
            startListening();
        }
    }

    public interface OnSpeechResultsListener
    {
        void onResults(ArrayList<String> results);
        void onPermissionRequests();
    }

    protected class SpeechRecognitionListener implements RecognitionListener
    {

        @Override
        public void onBeginningOfSpeech() {}

        @Override
        public void onBufferReceived(byte[] buffer)
        {

        }

        @Override
        public void onEndOfSpeech()
        {}

        @Override
        public synchronized void onError(int error)
        {

            if(error==SpeechRecognizer.ERROR_INSUFFICIENT_PERMISSIONS)
            {
                if(m_SpeechResults != null) {
                    m_SpeechResults.onPermissionRequests();
                }
                return;
            }
            if(error==SpeechRecognizer.ERROR_RECOGNIZER_BUSY)
            {
                if(m_SpeechResults!=null) {
                    ArrayList<String> errorList=new ArrayList<String>(1);
                    errorList.add("ERROR RECOGNIZER BUSY");
                    m_SpeechResults.onResults(errorList);
                }
                return;
            }

            if(error==SpeechRecognizer.ERROR_NO_MATCH)
            {
                if(m_SpeechResults!=null)
                    m_SpeechResults.onResults(null);
            }

            if(error==SpeechRecognizer.ERROR_NETWORK)
            {
                ArrayList<String> errorList=new ArrayList<String>(1);
                errorList.add("STOPPED LISTENING");
                if(m_SpeechResults!=null)
                    m_SpeechResults.onResults(errorList);
            }
            Log.d(TAG, "error = " + error);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    listenAgain();
                }
            },100);


        }

        @Override
        public void onEvent(int eventType, Bundle params)
        {

        }

        @Override
        public void onPartialResults(Bundle partialResults)
        {

        }

        @Override
        public void onReadyForSpeech(Bundle params) {}

        @Override
        public void onResults(Bundle results)
        {
            if(results!=null && m_SpeechResults!=null)
                m_SpeechResults.onResults(results.getStringArrayList(SpeechRecognizer.RESULTS_RECOGNITION));
            listenAgain();

        }

        @Override
        public void onRmsChanged(float rmsdB) {}

    }

    public boolean ismIsListening() {
        return m_bIsListening;
    }
}
