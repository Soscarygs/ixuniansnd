package com.ixuniansnd.ixuniansnd;

import android.app.Activity;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.support.v7.app.AppCompatActivity;

import android.widget.ScrollView;
import android.widget.TextView;
import com.ixuniansnd.ixuniansnd.PlayListFragment;
import com.ixuniansnd.ixuniansnd.ictPlaylistRecyclerViewAdapter;

import java.util.ArrayList;
import java.util.List;
import com.ixuniansnd.ixuniansnd.ictItemTouchHelperCallback;
import com.ixuniansnd.ixuniansnd.ictMediaFragmentBase;

/**
 * A simple {@link Fragment} subclass.
 * to handle interaction events.
 * Use the {@link ictPlayListView#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ictPlayListView extends ictMediaFragmentBase {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private volatile List<ictFileInfo> m_AudioInfo = null;
    MyPlayListRecyclerViewAdapter m_RecyclerViewAdapter = null;
    ictPlaylistRecyclerViewAdapter m_PlaylistListPlaylist= null;

    public interface OnGetMediaInfoInterface
    {
        List<ictFileInfo> getPlaylist();
        void GetPlaylistTitles(List<String> playlistTitles );
        void SelectPlaylist( String playlistName );
        boolean DeletePlaylist( String playlistName );
        String GetSelectedPlaylist();
        Context GetContext();
    }

    private PlayListFragment.OnListFragmentInteractionListener mListener;
    private OnGetMediaInfoInterface m_Context = null;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment ictPlayListView.
     */
    // TODO: Rename and change types and number of parameters
    public static ictPlayListView newInstance(List<ictFileInfo> a_AudioInfo, OnGetMediaInfoInterface a_Context) {

        ArrayList<ictFileInfo> audioArrayList = new ArrayList<ictFileInfo>(a_AudioInfo);

        ictPlayListView fragment = new ictPlayListView();
        Bundle args = new Bundle();
        args.putParcelableArrayList(PlayListFragment.PLAYLIST_KEY, audioArrayList);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            //m_AudioInfo = getArguments().getParcelableArrayList(PlayListFragment.PLAYLIST_KEY);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_main, container, false);

        RecyclerView recyclerView = (RecyclerView)rootView.findViewById(R.id.playlist_view);
        if(recyclerView != null)
        {
            Context context = recyclerView.getContext();
            recyclerView.setLayoutManager ( new LinearLayoutManager( context ));
            List<ictFileInfo> pCurrPlaylist = m_Context.getPlaylist();
            m_RecyclerViewAdapter = new MyPlayListRecyclerViewAdapter(MyPlayListRecyclerViewAdapter.MyPlaylistType.ICT_PLAYLIST, getActivity(), pCurrPlaylist, mListener);
            recyclerView.setAdapter( m_RecyclerViewAdapter );

            ItemTouchHelper.Callback itemTouchHelperCallback = new ictItemTouchHelperCallback(m_RecyclerViewAdapter);
            ItemTouchHelper itemTouchHelper = new ItemTouchHelper(itemTouchHelperCallback);
            itemTouchHelper.attachToRecyclerView(recyclerView);
        }

        RecyclerView playlistListPlaylistView = (RecyclerView)rootView.findViewById(R.id.playlist_list_playlist);
        if(playlistListPlaylistView != null)
        {
            Context context = playlistListPlaylistView.getContext();
            playlistListPlaylistView.setLayoutManager( new LinearLayoutManager( context ));
            List<String> allPlayLists = new ArrayList<String>();
            m_Context.GetPlaylistTitles(allPlayLists);
            m_PlaylistListPlaylist = new ictPlaylistRecyclerViewAdapter( allPlayLists, m_Context);
            playlistListPlaylistView.setAdapter( m_PlaylistListPlaylist );
        }
        return rootView;
    }

    public void updatePlaylist( List<ictFileInfo> playlist)
    {
        //if(mListener != null)
        //   mListener.SendSongList(playlist);
        if(m_RecyclerViewAdapter != null) {
            m_RecyclerViewAdapter.reInit(playlist);
        }
    }
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof PlayListFragment.OnListFragmentInteractionListener) {
            mListener = (PlayListFragment.OnListFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }

        try {
            // Required empty public constructor
            m_Context = (OnGetMediaInfoInterface)context;
        }catch(ClassCastException e){}
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    @Override
    public void onResume()
    {
        super.onResume();
        if(m_PlaylistListPlaylist != null) {
            List<String> allPlayLists = new ArrayList<String>();
            m_Context.GetPlaylistTitles(allPlayLists);
            m_PlaylistListPlaylist.reInit(allPlayLists);
        }
        if(m_RecyclerViewAdapter != null) {
            m_RecyclerViewAdapter.reInit(m_Context.getPlaylist());
        }
    }

    @Override
    public void onStart()
    {
        View view = getView();
        ScrollView recyclerView = (ScrollView)view.findViewById(R.id.playlist_scrollview);
        setController(view);

        RecyclerView playListList = (RecyclerView)view.findViewById(R.id.playlist_view);
        playListList.setOnTouchListener( new View.OnTouchListener()
        {
            public boolean onTouch(View view, MotionEvent event)
            {
                if(event.getAction() == MotionEvent.ACTION_MOVE) {
                    onPlaylistTouch();
                }
                return false;
            }
        });

        recyclerView.setOnTouchListener( new View.OnTouchListener()
        {
            public boolean onTouch(View v, MotionEvent event)
            {
                if(event.getAction() == MotionEvent.ACTION_MOVE)
                {
                    onPlaylistTouch();
                }
                return false;
            }
        });

        super.onStart();
    }

    private void setTrackIndex( int a_dwCurrentTrack)
    {
        if(a_dwCurrentTrack < m_AudioInfo.size()){
            setSong(m_AudioInfo.get(a_dwCurrentTrack));
        }
    }

    public void setSong(ictFileInfo a_FileInfo)
    {
        setSongIndex(a_FileInfo.getIndex());
    }
}
