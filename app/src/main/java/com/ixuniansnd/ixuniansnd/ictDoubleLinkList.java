package com.ixuniansnd.ixuniansnd;

import android.util.Log;

/**
 * Created by user2 on 6/29/2017.
 */

public class ictDoubleLinkList<E> {
    private ictNode m_Head = null;
    private ictNode m_Tail = null;
    private int m_Size;

    ictDoubleLinkList()
    {
        m_Size = 0;
    }

    public class ictNode {
        ictNode(E a_Element, ictNode a_Next, ictNode a_Prev) {
            m_Element = a_Element;
            m_Next= a_Next;
            m_Prev = a_Prev;
        }

        E m_Element;
        ictNode m_Prev;
        ictNode m_Next;
    }

    public int Size() { return m_Size; }
    public boolean IsEmpty() { return m_Size == 0; }
    public void AddFront(E a_Element)
    {
        ictNode node = new ictNode( a_Element, m_Head, null);
        if(m_Head != null)
        {
            m_Head.m_Prev = node;
        }
        m_Head = node;
        if(m_Tail == null) m_Tail = node;
        m_Size++;
    }

    public void AddBack(E a_Element)
    {
        ictNode node = new ictNode( a_Element, null, m_Tail );
        if(m_Tail != null) m_Tail.m_Next = node;
        m_Tail = node;
        if(m_Head == null) m_Head = node;
        m_Size++;
    }

    public class ictIterator
    {
        ictNode m_Next;
        ictNode m_Head = null;
        ictNode m_Tail = null;
        ictIterator( ictDoubleLinkList a_List )
        {
            m_Head = a_List.m_Head;
            m_Tail = a_List.m_Tail;
            m_Next = m_Head;

        }
        ictNode Get()
        {
            return m_Next;
        }
        ictNode Next()
        {
            if(m_Next != null)
                m_Next = m_Next.m_Next;
            return m_Next;
        }

        ictNode NextCycle()
        {
            if(m_Next != null)
                m_Next = m_Next.m_Next;
            if(m_Next == null)
                m_Next = m_Head;
            return m_Next;
        }

        ictNode Prev()
        {
            if(m_Next != null)
                m_Next = m_Next.m_Prev;
            return m_Next;
        }

        ictNode PrevCycle()
        {
            if(m_Next != null)
                m_Next = m_Next.m_Prev;
            if(m_Next == null)
                m_Next = m_Tail;
            return m_Next;
        }
        public ictNode SetPos( int pos )
        {
            if(pos >= m_Size)
            {
                return null;
            }
            ictNode pCurNode = m_Head;
            for( int i = 0; i < pos; i++)
            {
                if(pCurNode != null)
                    pCurNode = pCurNode.m_Next;
            }
            m_Next = pCurNode;
            return pCurNode;
        }

        public int GetPos()
        {
            ictNode pCurNode = m_Head;
            for( int i = 0; i < m_Size; i++) {
                if (pCurNode == m_Next)
                    return i;
                if(pCurNode != null)
                    pCurNode = pCurNode.m_Next;
            }
            return -1;
        }
    }

    ictIterator Iterator()
    {
        return new ictIterator(this);
    }

    public void Delete( ictNode a_Node )
    {
        if(a_Node != null)
        {
            if(a_Node == m_Tail)
                m_Tail = a_Node.m_Prev;
            if(a_Node == m_Head)
                m_Head = a_Node.m_Next;

            if(a_Node.m_Prev != null)
                a_Node.m_Prev.m_Next = a_Node.m_Next;
            if(a_Node.m_Next != null)
                a_Node.m_Next.m_Prev = a_Node.m_Prev;

            m_Size--;
        }
    }

    public void Free()
    {
        while(m_Tail != null) {
            //Print();
            Delete(m_Tail);
        }
    }

    public void InsertAtPos ( E a_Element, int pos )
    {
        ictNode pNewNode = new ictNode(a_Element, null,null);
        if(pos == 0)
        {
            AddFront(a_Element);
            return;
        }
        ictNode pPtr = m_Head;
        for( int i = 1; i <= m_Size; i++ )
        {
            if( i == pos)
            {
                ictNode pInserted = InsertAfter( pPtr, pNewNode );
            }
            if(pPtr != null)
                pPtr = pPtr.m_Next;
        }
    }

    public ictNode InsertAfter( ictNode a_Node, ictNode a_InsertedNode )
    {
        if(a_Node == null)
        {
            a_InsertedNode.m_Prev = a_Node;
            m_Head = a_InsertedNode;
            if(a_InsertedNode.m_Next !=null)
                a_InsertedNode.m_Next.m_Prev = a_InsertedNode;
            m_Size++;
            return a_InsertedNode;
        }
        a_InsertedNode.m_Next = a_Node.m_Next;
        a_InsertedNode.m_Prev = a_Node;
        if(a_Node.m_Next != null)
            a_Node.m_Next.m_Prev = a_InsertedNode;
        a_Node.m_Next = a_InsertedNode;
        if(m_Tail == a_Node)
            m_Tail = a_InsertedNode;
        m_Size++;
        return a_InsertedNode;
    }

    public void Print()
    {
        ictNode pForward = m_Head;
        while(pForward != null)
        {
            Log.i("IAN_", pForward.m_Element.toString());
            pForward = pForward.m_Next;
        }
        Log.i("IAN_","====Backward====");
        ictNode pBack = m_Tail;
        while(pBack != null)
        {
           Log.i("IAN_", pBack.m_Element.toString());
            pBack = pBack.m_Prev;
        }
        Log.i("IAN_", "Size is: " + m_Size);
    }
}

class LinkListTest
{
    public static void Test()
    {
        ictDoubleLinkList<Integer> db = new ictDoubleLinkList<Integer>();
        db.InsertAtPos( new Integer(7), 0);
        db.InsertAtPos( new Integer(8), 1);
        db.InsertAtPos( new Integer(9), 2);
        db.InsertAtPos( new Integer(11),0);
        db.InsertAtPos( new Integer(12),2);
        db.Print();

        ictDoubleLinkList<Integer> cy = new ictDoubleLinkList<Integer>();
        cy.InsertAtPos( new Integer(-1),0);
        cy.InsertAtPos( new Integer(-2),1);
        cy.InsertAtPos( new Integer(-3),2);
        cy.InsertAtPos( new Integer(-4),3);
        cy.Print();
        cy.Free();
        cy.InsertAtPos( new Integer(-17), 0);
        cy.InsertAtPos( new Integer(-18), 1);
        cy.InsertAtPos( new Integer(-19), 2 );
        cy.InsertAtPos( new Integer(-20), 3 );
        cy.Print();

        ictDoubleLinkList<Integer>.ictIterator itr = cy.Iterator();
        for ( int i = 0; i < 7; i++) {
            ictDoubleLinkList<Integer>.ictNode pNode = itr.Get();
            if (pNode != null)
                Log.i("IAN_", i + ": " + pNode.m_Element.toString());
            itr.NextCycle();
        }
        ictDoubleLinkList<Integer>.ictNode pCurr = itr.SetPos(0);
        if(pCurr != null)
            Log.i("IAN_", "Node 0 : " + pCurr.m_Element.toString());
        pCurr = itr.SetPos(2);
        if(pCurr != null)
            Log.i("IAN_", "Node 2 : " + pCurr.m_Element.toString());
        pCurr = itr.SetPos(3);
        if(pCurr != null)
            Log.i("IAN_", "Node 3 : " + pCurr.m_Element.toString());
        pCurr = itr.SetPos(4);
        if(pCurr != null)
            Log.i("IAN_", "Node 4 : " + pCurr.m_Element.toString());
        pCurr = itr.SetPos(5);
        if(pCurr != null)
            Log.i("IAN_", "Node 5 : " + pCurr.m_Element.toString());
    }

}
