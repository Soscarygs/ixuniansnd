package com.ixuniansnd.ixuniansnd;

import android.content.Context;
import android.view.Gravity;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.MediaController;


/**
 * Created by trong on 2/25/17.
 */

public class MediaUIController extends MediaController {
    ImageButton m_PlayTimesBtn = null;
    Context m_Context = null;
    int m_dwToggleIndex = 0;
    public interface MediaUIControllerAdditionalUI
    {
        public void SetLoopType ( int a_dwLoopType );
    }
    MediaUIControllerAdditionalUI m_AddUIController = null;
    public MediaUIController(Context context, MediaUIControllerAdditionalUI a_AddUIController)
    {
        super(context);
        m_Context = context;
        m_AddUIController = a_AddUIController;

        m_PlayTimesBtn = new ImageButton(m_Context);
        m_PlayTimesBtn.setImageResource(R.drawable.ic_repeat_black_16dp);
    }
    public void SetLoopType( int a_dwLoopType )
    {
        m_dwToggleIndex = a_dwLoopType;
        switch(m_dwToggleIndex)
        {
            case 0:
                m_PlayTimesBtn.setImageResource(R.drawable.ic_repeat_black_16dp);
                break;
            case 1:
                m_PlayTimesBtn.setImageResource(R.drawable.ic_repeat_one_black_16dp);
            default:
                break;
        }
    }

    public void setAnchorView( View view )
    {
        super.setAnchorView(view);
        FrameLayout.LayoutParams frameParams = new FrameLayout.LayoutParams(
                LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        frameParams.gravity = Gravity.RIGHT| Gravity.TOP;
        m_dwToggleIndex = 0;
        m_PlayTimesBtn.setOnClickListener( new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                m_dwToggleIndex = 1 - m_dwToggleIndex;
                SetLoopType(m_dwToggleIndex);
                m_AddUIController.SetLoopType(m_dwToggleIndex);
            }
        });
        addView(m_PlayTimesBtn, frameParams);


    }
}
