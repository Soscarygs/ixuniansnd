package com.ixuniansnd.ixuniansnd;

import android.content.Context;
import android.util.Log;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOError;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import com.ixuniansnd.ixuniansnd.ictFileInfo;

/**
 * Created by user2 on 6/6/2017.
 */

public class ictPlaylistData implements Serializable {
    String m_sId;
    String m_sDescription;
    public class ictPlaylistEntry implements Serializable
    {
        String m_Name = null;
        int m_dwIndex = -1;
    }

    public List<ictPlaylistEntry> m_MediaTitle = new ArrayList<ictPlaylistEntry>();

    private static final int serialVersionUID = 1;

    void SetDescription(String a_sDescription ) { m_sDescription = a_sDescription; }
    final String getDescription() { return m_sDescription; }
    void Add(String a_sNewMedia, int a_dwIndex)
    {
        ictPlaylistEntry pEntry = new ictPlaylistEntry();
        pEntry.m_dwIndex = a_dwIndex;
        pEntry.m_Name = a_sNewMedia;
        m_MediaTitle.add(pEntry);
    }
    @Override
    public  String toString() { return m_sId; }
}

class ictPlaylists implements Serializable
{

    public interface OnPlayListChangedEvent
    {
        void OnPlaylistChanged( ictPlaylistData playList );
    }

    ArrayList<ictPlaylistData> m_Playlists = new ArrayList<ictPlaylistData>();
    transient ictPlaylistData m_pPlaylistCurr = null;
    private OnPlayListChangedEvent m_Context = null;

    transient String m_sLastDeletedPlaylistId = "";
    transient ictPlaylistData.ictPlaylistEntry m_pLastDeletedEntry = null;
    transient int m_dwLastDeletedIdx = -1;
    transient String m_sLastPlaylistSelected = "";

    final Comparator<ictPlaylistData> m_SortPlaylistData = new Comparator<ictPlaylistData>()
    {
        @Override
        public int compare(ictPlaylistData obj1, ictPlaylistData obj2)
        {
            return obj1.m_sId.toLowerCase().compareTo(obj2.m_sId.toLowerCase());
        }
    };

    ictPlaylists( OnPlayListChangedEvent a_Context )
    {
        try{
            m_Context = a_Context;
        }catch(ClassCastException e)
        {}
    }
    void Serialize(String filePath, Context context)
    {
        if ( m_Playlists.size() == 0 )
            return;
        try
        {
            String fileFinalPath = context.getFilesDir().getPath().toString() + "/" + filePath;
            FileOutputStream fos = new FileOutputStream( fileFinalPath );
            ObjectOutputStream ous = new ObjectOutputStream( fos );
            ous.writeObject(m_Playlists);
            boolean bHasCurr = m_pPlaylistCurr != null;
            ous.writeBoolean(bHasCurr);
            if(bHasCurr)
                ous.writeObject(m_pPlaylistCurr.m_sId);
            ous.flush();
            ous.close();
            fos.close();
        }catch(IOException e)
        {
            Log.e("IANE_", e.toString());
        }
        catch(Exception e)
        {
            Log.e("IANE_", e.toString());
        }
    }

    void Deserialize(String a_fileData, Context context)
    {
        try
        {
            String fileFinalPath = context.getFilesDir().getPath() + "/" + a_fileData;
            FileInputStream fis = new FileInputStream(fileFinalPath);
            ObjectInputStream ois = new ObjectInputStream( fis );
            m_Playlists = (ArrayList<ictPlaylistData>)ois.readObject();
            boolean bHasCurr = ois.readBoolean();
            if(bHasCurr) {
                String sCurrPlayList = (String)ois.readObject();
                if (m_Playlists.size() > 0) {
                    for( ictPlaylistData aPlayList : m_Playlists)
                        if( aPlayList.m_sId.equalsIgnoreCase(sCurrPlayList)) {
                            m_pPlaylistCurr = aPlayList;
                            break;
                        }
                }
            }
            ois.close();
            fis.close();

            Collections.sort(m_Playlists, m_SortPlaylistData);
        }
        catch(FileNotFoundException e) {
            Log.e("IANE_", e.toString());
        }
        catch(ClassNotFoundException e)
        {
            Log.e("IANE_", e.toString());
        }
        catch(IOException e)
        {
            Log.e("IANE_", e.toString());
        }
        catch(ClassCastException e)
        {
            Log.e("IANE_", e.toString());
        }
    }

    ictPlaylistData CreatePlaylist( String sPlaylistName, String sPlaylistDescription, ictFileInfo songInfo )
    {
        for(int i=0; i < m_Playlists.size(); i++)
        {
            ictPlaylistData pPlaylist = m_Playlists.get(i);
            if(pPlaylist.m_sId.equalsIgnoreCase( sPlaylistName)) {
                AddToPlaylist(pPlaylist.m_sId, songInfo.GetFileName(), songInfo.getIndex());
                return pPlaylist;
            }
        }
        ictPlaylistData pData = new ictPlaylistData();
        pData.m_sId = sPlaylistName;
        m_Playlists.add( pData );
        Collections.sort(m_Playlists, m_SortPlaylistData);
        AddToPlaylist( pData.m_sId, songInfo.GetFileName(), songInfo.getIndex());
        m_pPlaylistCurr = pData;
        return m_pPlaylistCurr;
    }

    ictPlaylistData GetPlaylistByName(String playlistName)
    {
        for( ictPlaylistData playlist : m_Playlists)
        {
            if(playlist.m_sId.equalsIgnoreCase(playlistName))
                return playlist;
        }
        return null;
    }

    void GetPlaylistEntries ( List<String> playlists)
    {
        for(int i =0; i < m_Playlists.size(); i++)
            playlists.add(m_Playlists.get(i).m_sId);
    }

    String GetPlaylistLastSelected()
    {
        return m_sLastPlaylistSelected;
    }

    void AddToPlaylist ( String playlistName, String songName, int a_dwSongIndex )
    {
        ictPlaylistData pPlaylistData = null;
        for(int i = 0; i < m_Playlists.size(); i++)
            if(m_Playlists.get(i).m_sId.equalsIgnoreCase(playlistName))
            {
                ictPlaylistData.ictPlaylistEntry pEntry = new ictPlaylistData().new ictPlaylistEntry();
                pEntry.m_Name = songName;
                pEntry.m_dwIndex = a_dwSongIndex;
                m_Playlists.get(i).m_MediaTitle.add(pEntry);
                pPlaylistData = m_Playlists.get(i);
                m_sLastPlaylistSelected = playlistName;
                break;
            }
        if(pPlaylistData != null) {
            m_pPlaylistCurr = pPlaylistData;
            m_Context.OnPlaylistChanged(pPlaylistData);
        }
    }

    ictPlaylistData getCurrentPlaylist()
    {
        return m_pPlaylistCurr;
    }
    boolean DeleteSongFromCurrPlaylist(ictFileInfo a_FileInfo)
    {
        ictPlaylistData currPlayList = getCurrentPlaylist();
        if( currPlayList != null)
        {
            ictPlaylistData.ictPlaylistEntry pToRemove = null;
            for ( ictPlaylistData.ictPlaylistEntry pEntry : currPlayList.m_MediaTitle )
            {
                if(pEntry.m_dwIndex == a_FileInfo.getIndex())
                {
                    pToRemove = pEntry;
                    break;
                }
            }
            if(pToRemove != null) {
                m_sLastDeletedPlaylistId = currPlayList.m_sId;
                m_pLastDeletedEntry = pToRemove;
                m_dwLastDeletedIdx = currPlayList.m_MediaTitle.indexOf(pToRemove);

                return currPlayList.m_MediaTitle.remove(pToRemove);
            }
            return false;
        }
        return ( false );
    }

    public void SelectPlaylist(String playlistName)
    {
        for( ictPlaylistData aPlaylist : m_Playlists)
            if(aPlaylist.m_sId.equalsIgnoreCase(playlistName))
            {
                m_pPlaylistCurr = aPlaylist;
                m_Context.OnPlaylistChanged(aPlaylist);
                break;
            }
    }
    public String GetSelectedPlaylist()
    {
        if(m_pPlaylistCurr != null)
        {
            return m_pPlaylistCurr.m_sId;
        }
        return null;
    }

    public boolean DeletePlaylist( String playlistName )
    {
        ictPlaylistData pToFind = null;
        for( ictPlaylistData  aPlaylist : m_Playlists)
            if(aPlaylist.m_sId.equalsIgnoreCase(playlistName))
            {
                pToFind = aPlaylist;
                break;
            }

        if(pToFind != null) {
            m_pPlaylistCurr = null;
            m_Context.OnPlaylistChanged(null);
            return m_Playlists.remove(pToFind);
        }
        return false;
    }

    public boolean MoveItemInPlayList(int a_dwFromPos, int a_dwToPos) {
        ictPlaylistData pCurrPlaylist = getCurrentPlaylist();
        if (pCurrPlaylist != null) {
            if(a_dwFromPos < a_dwToPos)
            {
                for(int i = a_dwFromPos; i < a_dwToPos; i++ )
                    Collections.swap(pCurrPlaylist.m_MediaTitle, i, i+1 );
            }else
            {
                for(int i = a_dwFromPos; i > a_dwToPos; i--)
                    Collections.swap(pCurrPlaylist.m_MediaTitle, i, i-1 );
            }
            //m_Context.OnPlaylistChanged(pCurrPlaylist);
            return true;
        }
        return false;
    }

    public boolean UndoLastDeleted() {
        if (m_pLastDeletedEntry != null && m_sLastDeletedPlaylistId != null && m_dwLastDeletedIdx != -1) {
            ictPlaylistData playlist = GetPlaylistByName(m_sLastDeletedPlaylistId);
            if ( playlist != null ) {
                playlist.m_MediaTitle.add(m_dwLastDeletedIdx, m_pLastDeletedEntry);
                m_sLastDeletedPlaylistId = null;
                m_pLastDeletedEntry = null;
                m_dwLastDeletedIdx = -1;
                m_Context.OnPlaylistChanged(playlist);
                return true;
            }
        }
        return false;
    }


}

