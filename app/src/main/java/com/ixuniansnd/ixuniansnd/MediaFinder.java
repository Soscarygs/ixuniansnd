package com.ixuniansnd.ixuniansnd;

import android.content.Context;
import android.database.Cursor;
import android.provider.MediaStore;
import android.util.Log;
import com.ixuniansnd.ixuniansnd.ictFileInfo;

import java.util.Collections;
import java.util.List;
import java.util.Comparator;

/**
 * Created by trong on 2/13/17.
 */

public class MediaFinder {

    static class SortAscendingName implements Comparator<ictFileInfo>
    {
        public int compare ( ictFileInfo b, ictFileInfo c)
        {
            return b.GetFileName().compareTo(c.GetFileName());
        }
    }

    public static void parseAllAudio( Context context, List<ictFileInfo> fileInfo)
    {
        try {
            int index = 0;
            String [] STAR = { "*"};
            String TAG = "Audio";
            String selection = MediaStore.Audio.Media.IS_MUSIC + " != 0";
            Cursor cur = context.getContentResolver().query(MediaStore.Audio.Media.EXTERNAL_CONTENT_URI, STAR, selection, null, null);
            if (cur == null) {
                // Failed
            } else if (!cur.moveToFirst()) {
                cur.close();
            } else {
                do {
                    int titleColumn = cur.getColumnIndex(MediaStore.Audio.Media.TITLE);
                    int filePathIndex = cur.getColumnIndexOrThrow(MediaStore.Audio.Media.DATA);

                    String title = cur.getString(titleColumn);
                    String filePath = cur.getString(filePathIndex);

                    ictFileInfo pFileInfo = new ictFileInfo();
                    pFileInfo.SetFileName( title );
                    pFileInfo.SetFullPath( filePath);
                    pFileInfo.setIndex( index++ );
                    fileInfo.add( pFileInfo );
                    Log.d("IxuniaSnd", title + " : " + filePath + "index: " + new Integer(pFileInfo.getIndex()).toString());
                } while (cur.moveToNext());
                cur.close();
            }
        }catch(Exception e)
        {
            e.printStackTrace();
        }
        Collections.sort(fileInfo, new SortAscendingName());
    }
}
