package com.ixuniansnd.ixuniansnd;


import android.app.Activity;
import android.content.Context;
import android.content.ContextWrapper;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.MediaController;
import android.widget.SeekBar;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;


public class ictMediaFragmentBase extends Fragment implements MediaController.MediaPlayerControl, MediaUIController.MediaUIControllerAdditionalUI
{
    public static final int s_PlayerShowDuration = 5000;
    protected MediaUIController m_Controller = null;
    private boolean m_bPauseOutOfView = false;
    protected OnListFragmentInteractionListener mListener;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public ictMediaFragmentBase() {
    }

    public void onDestroyView()
    {
        super.onDestroyView();
    }

    public void hideController()
    {
        if(m_Controller != null)
        {
            if(m_Controller.isShowing())
                m_Controller.hide();
        }
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        if(hidden)
            hideController();
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if(!isVisibleToUser)
            hideController();
        else
        {
            if(m_bPauseOutOfView) {
                start();
                m_bPauseOutOfView = false;
            }
        }
    }

    public void onStop()
    {
        m_Controller.hide();
        m_Controller.setEnabled(false);
        m_Controller = null;
        super.onStop();
    }

    public void onResume()
    {
        m_Controller.setEnabled(true);
        super.onResume();
    }

    public void onPause()
    {
        m_Controller.hide();
        m_Controller.setEnabled(false);
        super.onPause();
    }

    public void onDestroy()
    {
        super.onDestroy();
    }

    public void onPlaylistTouch()
    {
        if(m_Controller != null ) {
            if(m_Controller.isEnabled())
                m_Controller.show( s_PlayerShowDuration);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnListFragmentInteractionListener) {
            mListener = (OnListFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnListFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnListFragmentInteractionListener extends ictSoundModule.ictSoundModuleInterface {
        // TODO: Update argument type and name
        void onListFragmentInteraction(ictFileInfo item, int pos);
        void SendSongList(List<ictFileInfo> a_ListInfo, int pos);
        int GetServiceCurrentTrackPos();
        int GetLoopType();
        boolean DeleteMedia( int pos );
    }

    private int getSoftbuttonsbarHeight(Activity activity) {

        if(Build.VERSION.SDK_INT >= 31)
        {
            return 0;
        }
        // getRealMetrics is only available with API 17 and +
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            DisplayMetrics metrics = new DisplayMetrics();
            activity.getWindowManager().getDefaultDisplay().getMetrics(metrics);
            int usableHeight = metrics.heightPixels;
            activity.getWindowManager().getDefaultDisplay().getRealMetrics(metrics);
            int realHeight = metrics.heightPixels;
            if (realHeight > usableHeight)
                return realHeight - usableHeight;
            else
                return 0;
        }
        return 0;
    }

    public Activity getActivity( Context context )
    {
        if( context instanceof Activity)
            return (Activity)context;
        else
        {
            while (context instanceof ContextWrapper) {
                if (context instanceof Activity) {
                    return (Activity)context;
                }
                context = ((ContextWrapper)context).getBaseContext();
            }
            return null;
        }
    }

    protected void setController(View a_View)
    {
        m_Controller = new MediaUIController(a_View.getContext(), this);
        m_Controller.SetLoopType( mListener.GetLoopType() );
        m_Controller.setPrevNextListeners( new View.OnClickListener()
        {
            public void onClick(View view){
                playNext();
            }
        }, new View.OnClickListener() {
            public void onClick(View view) {
                playPrev();
            }
        });

        m_Controller.setMediaPlayer(this);
        m_Controller.setAnchorView(a_View);
        Activity host = getActivity(a_View.getContext());
        if ( host != null )
            m_Controller.setPadding(0,0,0,getSoftbuttonsbarHeight(host));
    }

    private void playNext() { mListener.playNext(); }
    private void playPrev() { mListener.playPrev(); }
    public boolean canPause(){ return true; }
    public boolean canSeekForward() { return true; }
    public boolean canSeekBackward() { return true; }
    public int getCurrentPosition() { return mListener.getCurrentPosition(); }
    public int getDuration() { return mListener.getDuration(); }
    public boolean isPlaying() { return mListener.isPlaying(); }

    public void pauseCommon()  { mListener.pause(); }
    public void SetLoopType( int a_dwLoopType )
    {
        mListener.SetLoopType( a_dwLoopType );
    }

    public void pause() {
        if(mListener.isPlaying())
        {
            pauseCommon();
        }
        m_bPauseOutOfView = false;
    }

    public void pauseOnDifferentView()
    {
        /***
         if(mListener.isPlaying())
         {
         pauseCommon();
         m_bPauseOutOfView = true;
         }
         */
    }

    public void seekTo(int pos)
    {
        mListener.seekTo(pos);
    }
    public void start() { mListener.start(); }
    public int getAudioSessionId() { return mListener.getAudioSessionId(); }

    public int getBufferPercentage()
    {
        return 0;
    }
    public void setSongIndex(int index) { mListener.setSongIndex( index ); }


    public void setSong(ictFileInfo a_FileInfo)
    {
        setSongIndex(a_FileInfo.getIndex());
    }

    public void onPrepare( int a_dwAudioSessionId, int a_dwSongIndex )
    {
        if(m_Controller != null && m_Controller.isEnabled()) {
            m_Controller.show( s_PlayerShowDuration );
        }
    }

    public void onCompletion ( int a_dwAudioSessionId )
    {
    }
}