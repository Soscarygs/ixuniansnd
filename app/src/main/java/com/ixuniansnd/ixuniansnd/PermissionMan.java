package com.ixuniansnd.ixuniansnd;

import android.Manifest;
import android.app.Activity;
import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.widget.Toast;

import java.util.*;

/**
 * Created by Ian on 2/4/17.
 */

public class PermissionMan {
    public static final int PERMISSIONS_ALL = 0;
    public static final int PERMISSION_READ_STORAGE = 1;
    public static final int PERMISSION_RECORD_AUDIO = 2;

    public static class PermissionData
    {
        PermissionData( String a_Permission, String a_Reason )
        {
            m_Permission = a_Permission;
            m_Reason = a_Reason;
        }
        String m_Permission;
        String m_Reason;
    }


    public interface OnPermissionEvent
    {
        void OnPermissionReceive( String [] dwPermissionType, int [] a_bGranted );
    }

    private HashMap<Integer, OnPermissionEvent> permissionsCallback = new <Integer, OnPermissionEvent>HashMap();


    public void Register( int slot, OnPermissionEvent event ){
        permissionsCallback.put(new Integer(slot), event);
    }

    public boolean HasPermission(Activity context, String permission )
    {
        if(ContextCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
            // Should we show an explanation ?
            if(ActivityCompat.shouldShowRequestPermissionRationale(context,permission)) {
                // Show an explanation async
            }
            else
            {
                int dwPermissionCode = PERMISSION_READ_STORAGE;
                if(permission.equals(Manifest.permission.READ_EXTERNAL_STORAGE)) {
                    dwPermissionCode = PERMISSION_READ_STORAGE;
                } else if ( permission.equals(Manifest.permission.RECORD_AUDIO))
                {
                    dwPermissionCode = PERMISSION_RECORD_AUDIO;
                }
                ActivityCompat.requestPermissions(context, new String[]{permission}, dwPermissionCode);
            }
        }
        return true;
    }

    public boolean HasPermissions(Activity context, PermissionData [] permissions, int a_dwPermissionCode )
    {
        ArrayList<PermissionData> permissionNeeded = new ArrayList<PermissionData>();
        for( int i = 0; i < permissions.length; i++)
        {
            if(ContextCompat.checkSelfPermission(context, permissions[i].m_Permission) != PackageManager.PERMISSION_GRANTED)
            {
                permissionNeeded.add(permissions[i]);
            }
        }
        if ( permissionNeeded.size() == 0)
            return true;

        ArrayList<String> requestPerm = new ArrayList<String>();
        String reasons = "";
        boolean bPromptReason = false;
        for( int i = 0; i < permissionNeeded.size(); i++) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(context, permissionNeeded.get(i).m_Permission)) {
                reasons += permissionNeeded.get(i).m_Reason + "\n";
                bPromptReason = true;
            }
            else
            {
                requestPerm.add(permissionNeeded.get(i).m_Permission);
            }
        }
        if(bPromptReason)
        {
            return false;
        }
        else
        {
            ActivityCompat.requestPermissions(context, requestPerm.toArray( new String[0]), a_dwPermissionCode);
            return false;
        }
    }

    public void TriggerEvent( int requestCode, String [] permissions, int [] grantResults )
    {
        if(permissionsCallback.containsKey(requestCode)) {
            OnPermissionEvent event = permissionsCallback.get(requestCode);
            if(event != null)
                event.OnPermissionReceive( permissions, grantResults );
        }
    }

    public void onRequestPermissionsResult( int requestCode, String [] permissions, int [] grantResults){

        if(grantResults.length > 0)
            TriggerEvent( requestCode, permissions, grantResults);
    }

    public static boolean CheckDrawOverlayPermission( Activity activity, int dwRequestCode )
    {
        if(Build.VERSION.SDK_INT < Build.VERSION_CODES.M)
        {
            return true;
        }
        if(!Settings.canDrawOverlays(activity))
        {
            Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION,
                    Uri.parse("package:" + activity.getPackageName()));
            activity.startActivityForResult(intent, dwRequestCode);
            return ( false );
        }
        return true;
    }

    public static boolean isMyServiceRunning(Activity activity, Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) activity.getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }
}
