package com.ixuniansnd.ixuniansnd;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import android.Manifest;
import android.app.Activity;
import android.app.SearchManager;
import android.content.BroadcastReceiver;
import android.content.ClipData;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.os.Messenger;
import android.os.Message;
import android.os.RemoteException;
import android.provider.Settings;
import android.speech.RecognizerIntent;
import android.support.design.widget.TabLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.MenuItemCompat;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import android.widget.TextView;
import android.widget.Toast;
import com.ixuniansnd.ixuniansnd.ictSpeechModule;
import com.ixuniansnd.ixuniansnd.ictPreferences;
import com.ixuniansnd.ixuniansnd.ictAudioVisualizer;
import com.ixuniansnd.ixuniansnd.ictAudioVisualizerProducer;
import com.ixuniansnd.ixuniansnd.ictPlaylists;
import com.ixuniansnd.ixuniansnd.ictPlayListView;
import com.ixuniansnd.ixuniansnd.ictDoubleLinkList;

public class MainActivity extends AppCompatActivity implements PlayListFragment.OnListFragmentInteractionListener,
    VoiceFragment.OnRequestPauseListener, SearchView.OnQueryTextListener, ictSoundModule.ictSoundModuleInterface,
        ictAudioVisualizerProducer.onDataReceivedInterface, ictPlaylists.OnPlayListChangedEvent, ictPlayListView.OnGetMediaInfoInterface
{

    static final int REQUEST_CODE = 0x00ff;

    PermissionMan m_PermissionMan = new PermissionMan();
    List<ictFileInfo> m_AudioInfo = new ArrayList<ictFileInfo>();
    Messenger m_Service = null;
    boolean m_bIsBoundToSoundService = false;
    ictSoundModule.ictSoundModuleInterface m_SoundService = null;
    ictPreferences m_Preferences = null;

    Menu m_SettingsMenu = null;
    private ictAudioVisualizerProducer m_Visualizer = null;
    int m_dwFileInfoIndex = -1;
    int m_dwAudioSessionId = -1;
    int m_dwServiceAudioTrackPos = 0;
    List<ictFileInfo> m_ServiceMediaList = null;

    ictPlaylists m_Playlists = null;

    public static final String AUDIO_INFO = "ictAudioInfo";
    public static final String FILEINFO_INDEX = "ictFileInfoIndex";
    public static final String FILEINFO_AUDIOSESSION = "ictFileInfoAudioSessionId";
    public static final String AUDIO_TRACK_POS = "ictServiceMediaTracPos";
    public static final String SERVICE_MEDIA_LIST = "ictServiceMediaList";
    public static final String LOOP_TYPE = "ictPlaylistLoopType";

    class IncomingHandler extends Handler
    {

        MainActivity m_Parent = null;
        IncomingHandler(MainActivity a_Parent)
        {
            super();
            this.m_Parent = a_Parent;
        }

        @Override
        public void handleMessage(Message msg)
        {
            switch(msg.what)
            {
                case ictSndListenerService.MSG_SEND_SOUND_INTERFACE: {
                    Bundle bundle = msg.getData();
                    try {
                        m_SoundService = bundle.getParcelable("SoundInterface");
                    } catch (ClassCastException e) {

                    }
                }
                    break;
                case ictSndListenerService.MSG_SOUND_PREPARE:
                        onPrepare( msg.arg1, msg.arg2 );
                    break;
                case ictSndListenerService.MSG_ON_SONG_COMPLETION:
                        onCompletion ( msg.arg1 );
                    break;
                case ictSndListenerService.MSG_SET_VALUE:
                    Log.i("IAN_", "Got value from service " + msg.arg1);
                    break;
                case ictSndListenerService.MSG_SET_STRING: {
                    Bundle bundle = msg.getData();
                    ArrayList<String> results = bundle.getStringArrayList(ictSndListenerService.VOICE_RESULT);
                    VoiceFragment voiceFrag = GetVoice();
                    if(voiceFrag != null) {
                        voiceFrag.setStatus(results);
                    }
                    break;
                }
                case ictSndListenerService.MSG_REQUEST_AUDIO_PERMISSION:
                    m_Parent.requestAudioPermission();
                    break;
                case ictSndListenerService.MSG_SET_RESULT:
                {
                    Bundle bundle = msg.getData();
                    ArrayList<String> results = bundle.getStringArrayList(ictSndListenerService.VOICE_RESULT);
                    VoiceFragment voiceFrag = GetVoice();
                    if(voiceFrag != null) {
                        if ( voiceFrag.processResults(results) )
                        {
                            onStopListeningRequest();
                        }
                    }
                }
                break;
                default:
                    super.handleMessage( msg );
            }
        }
    }

    final Messenger m_Messenger = new Messenger( new IncomingHandler(this));
    private ServiceConnection m_Connection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service)
        {
            m_Service = new Messenger(service);
            try
            {
                Message msg = Message.obtain(null, ictSndListenerService.MSG_REGISTER_CLIENT, this.hashCode(), 0);
                Bundle bundle = new Bundle();
                bundle.putParcelable(ictPreferences.PREF_NAME, m_Preferences);
                msg.setData(bundle);
                msg.replyTo = m_Messenger;
                m_Service.send(msg);

                msg = Message.obtain(null, ictSndListenerService.MSG_SET_LOOP_TYPE);
                msg.arg1 = m_Preferences.m_dwLoopType;
                msg.replyTo = m_Messenger;
                m_Service.send(msg);

                if(m_ServiceMediaList != null)
                {
                    SendMediaToServer(m_ServiceMediaList, m_dwServiceAudioTrackPos);
                    m_ServiceMediaList = null;
                    m_dwServiceAudioTrackPos = 0;
                }


                /**
                int songIndex = 0;
                for(int i = 0; i < m_AudioInfo.size(); i++)
                    if(m_AudioInfo.get(i).getIndex() == m_dwFileInfoIndex)
                        songIndex = i;

                SendMediaToServer(m_AudioInfo, songIndex);
                */
                msg = Message.obtain(null, ictSndListenerService.MSG_SET_VALUE,this.hashCode(), 0 );
                msg.replyTo = m_Messenger;
                m_Service.send(msg);

            }catch(RemoteException e)
            {

            }
            //Toast.makeText(MainActivity.this, R.string.remote_service_connected, Toast.LENGTH_SHORT).show();

        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            m_Service = null;
            //Toast.makeText(MainActivity.this, R.string.remote_service_disconnected, Toast.LENGTH_SHORT).show();
        }
    };

    void doBindService()
    {
        bindService( new Intent(MainActivity.this, ictSndListenerService.class), m_Connection, Context.BIND_AUTO_CREATE);
        m_bIsBoundToSoundService = true;
    }

    void doUnBindService( boolean bAppDestroyed )
    {
        if(m_bIsBoundToSoundService)
        {
            if(m_Service != null)
            {
                try {
                    Message msg = Message.obtain(null, bAppDestroyed ?
                            ictSndListenerService.MSG_UNREGISTER_CLIENT_FINAL
                            : ictSndListenerService.MSG_UNREGISTER_CLIENT);
                    msg.replyTo = m_Messenger;
                    m_Service.send(msg);
                }catch(RemoteException e)
                {

                }
            }
            unbindService(m_Connection);
            m_bIsBoundToSoundService = false;
        }
    }
    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    private SectionsPagerAdapter mSectionsPagerAdapter;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager mViewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        LinkListTest.Test();

        m_Playlists = new ictPlaylists(this);

        m_Preferences = new ictPreferences(this);
        m_Preferences.RestorePreferences();


        boolean bGotSaved = false;
        if(savedInstanceState != null)
        {
            //m_AudioInfo = savedInstanceState.getParcelableArrayList(AUDIO_INFO);
            m_dwFileInfoIndex = savedInstanceState.getInt(FILEINFO_INDEX, -1);
            m_dwAudioSessionId = savedInstanceState.getInt(FILEINFO_AUDIOSESSION, -1 );

            m_dwServiceAudioTrackPos = savedInstanceState.getInt(AUDIO_TRACK_POS, 0);
            m_ServiceMediaList = savedInstanceState.getParcelableArrayList(SERVICE_MEDIA_LIST);
            m_Preferences.m_dwLoopType = savedInstanceState.getInt(LOOP_TYPE, 0);
            //bGotSaved = true;
        }


        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        PermissionMan.PermissionData [] permissions = {
                new PermissionMan.PermissionData(Manifest.permission.READ_EXTERNAL_STORAGE, getString(R.string.PermissionReadExternalStorage))
                ,new PermissionMan.PermissionData(Manifest.permission.WRITE_EXTERNAL_STORAGE, getString(R.string.PermissionWriteExternalStorage))
                ,new PermissionMan.PermissionData(Manifest.permission.RECORD_AUDIO,  getString(R.string.PermissionRecordAudio))
        };

        m_PermissionMan.Register(PermissionMan.PERMISSIONS_ALL, new AllPermissionsEvent(this));
        boolean bGotPermission = m_PermissionMan.HasPermissions(this, permissions, PermissionMan.PERMISSIONS_ALL);
        if( bGotPermission )
        {
            //if(!bGotSaved)
            {
                MediaFinder.parseAllAudio(this, m_AudioInfo);
            }
            bGotPermission = true;
        }
        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager(), m_AudioInfo, this);

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);

        /*
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
        */

        if(bGotPermission) {
           Intent startSndListenerServiceInt = new Intent(this, ictSndListenerService.class);
           startService(startSndListenerServiceInt);
           doBindService();
        }

        //handleIntent(getIntent());

        Intent detailsIntent =  new Intent(RecognizerIntent.ACTION_GET_LANGUAGE_DETAILS);

        detailsIntent.setPackage("com.google.android.googlequicksearchbox");

        PackageManager packageManager = getPackageManager();

        for (PackageInfo packageInfo: packageManager.getInstalledPackages(0)) {
            if (packageInfo.packageName.contains("com.google.android.googlequicksearchbox"))
                Log.d("AAA", packageInfo.packageName + ", "  + packageInfo.versionName);
        }

        sendOrderedBroadcast(
                detailsIntent, null, new ictLanguageDetailsChecker2(), null, Activity.RESULT_OK, null, null);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState)
    {
        super.onSaveInstanceState(outState);
        if(m_AudioInfo != null) {
            outState.putParcelableArrayList(AUDIO_INFO, (ArrayList<ictFileInfo>) m_AudioInfo);
            List<ictFileInfo> mediaList = GetServiceMediaList();
            if(mediaList != null) {
                outState.putParcelableArrayList(SERVICE_MEDIA_LIST, (ArrayList<ictFileInfo>) mediaList);
                int trackPos = GetServiceCurrentTrackPos();
                outState.putInt(AUDIO_TRACK_POS, trackPos);
            }
            int loopType = GetLoopType();
            outState.putInt(LOOP_TYPE, loopType);
            outState.putInt(FILEINFO_INDEX, m_dwFileInfoIndex);
            if(isPlaying()) {
                outState.putInt(FILEINFO_AUDIOSESSION, getAudioSessionId());
            }
        }
    }

    @Override
    protected void onNewIntent(Intent intent)
    {
        super.onNewIntent(intent);
        handleIntent(intent);

    }

    private void handleIntent( Intent intent)
    {
        if(Intent.ACTION_SEARCH.equals(intent.getAction()))
        {
            Log.i("IAN_", "handleIntent");
            String query = intent.getStringExtra(SearchManager.QUERY);
            Query(query, this);
        }
    }

    @Override
    public void onDestroy()
    {
        boolean bIsFinishing = isFinishing();
        if ( bIsFinishing ) {
            if (m_Visualizer != null)
                m_Visualizer.onRelease();
            m_Visualizer = null;
        }

        doUnBindService( isFinishing() );
        super.onDestroy();
    }

    @Override
    public void onResume()
    {
        super.onResume();
        m_Playlists.Deserialize("ixunian_playlists.bin", this);
        if(m_Visualizer != null)
            m_Visualizer.SetEnable(true);
        else
        {
            if(m_dwAudioSessionId != -1)
            {
                if(m_Visualizer == null)
                {
                    m_Visualizer = new ictAudioVisualizerProducer(this);
                }

                try {
                    m_Visualizer.Reset(m_dwAudioSessionId);
                    m_Visualizer.SetEnable(true);
                }catch(IllegalStateException e)
                {
                    Log.e("IANE_", e.toString());
                }
                m_dwAudioSessionId = -1;
            }
        }
    }
    @Override
    public void onPause(){
        if(m_Visualizer != null)
            m_Visualizer.SetEnable(false );
        onStopListeningRequest();
        m_Playlists.Serialize("ixunian_playlists.bin", this);
        super.onPause();
    }

    @Override
    public void onStop()
    {
        super.onStop();
        m_Preferences.WritePreferences();
    }

    private void SendMediaToServer(List<ictFileInfo> songList, int pos)
    {
        Log.i("IAN_","1. SendMediaToServer list" + songList.size() + " pos = " + pos);
        if(m_bIsBoundToSoundService && m_Service != null ) {
            try {
                Message msgToServ = Message.obtain(null, ictSndListenerService.MSG_POPULATE_MEDIA);
                Bundle bundle = new Bundle();
                bundle.putParcelableArrayList("Songs", (ArrayList<ictFileInfo>) songList);
                bundle.putInt("SongPosIdx", pos);
                Log.i("IAN_", "2. SendMediaToServer actual sent " + pos);
                msgToServ.setData(bundle);
                m_Service.send(msgToServ);

            }
            catch(RemoteException e)
            {}
        }
    }

    protected void RePopulateSongList()
    {
        PlayListFragment songList = GetAudio();
        if(songList != null)
            songList.reInit(1, m_AudioInfo);
    }

    protected void RePopulatePlayList()
    {
        ictPlayListView playlist = GetPlaylistViewFrag();
        if(playlist != null) {
            List<ictFileInfo> pCurrPlaylist = getPlaylist();
            playlist.updatePlaylist(pCurrPlaylist);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
            MenuItem searchItem = menu.findItem(R.id.search);
            SearchView searchView = (SearchView) searchItem.getActionView();
            searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
            searchView.setIconifiedByDefault(false);
            MenuItemCompat.setOnActionExpandListener(searchItem, new MenuItemCompat.OnActionExpandListener()
            {
               @Override
                public boolean onMenuItemActionExpand(MenuItem item)
               {
                   PlayListFragment playList = GetAudio();
                   if(playList != null)
                       playList.hideController();
                   return true;
               }

               @Override
               public boolean onMenuItemActionCollapse(MenuItem item)
               {
                   PlayListFragment songList = GetAudio();
                   ictPlayListView playlist = GetPlaylistViewFrag();
                   if(songList != null && songList.isVisible())
                        RePopulateSongList();
                   else if(playlist != null && playlist.isVisible())
                   {
                       List<ictFileInfo> pCurrplaylist = getPlaylist();
                       playlist.updatePlaylist(pCurrplaylist);
                   }
                   return true;
               }
            });
            searchView.setOnQueryTextListener(this);

            MenuItem m_uiTTS = menu.findItem(R.id.action_tts);
            if(m_uiTTS != null)
            {
                m_uiTTS.setChecked(m_Preferences.m_bTTS);
            }
        }
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu)
    {
        m_SettingsMenu = menu;
        return super.onPrepareOptionsMenu(menu);

    }
    @Override
    public boolean onQueryTextSubmit(String query) {
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            try {
                if( m_SettingsMenu != null) {
                    MenuItem searchItem = m_SettingsMenu.findItem(R.id.search);
                    SearchView searchView = (SearchView) searchItem.getActionView();
                    if( !searchView.isIconified())
                        MenuItemCompat.collapseActionView(searchItem);
                }
            }catch(Exception e)
            {

            }
        }
        return true;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        if(newText != null && newText.length() > 0 )
        {
            Query(newText, this);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        switch(id)
        {
            case R.id.action_tts: {
                    if (item.isChecked())
                        item.setChecked(false);
                    else
                        item.setChecked(true);
                    SetTTS( item.isChecked());
                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        private List<ictFileInfo> m_FileInfo = null;
        ictPlayListView.OnGetMediaInfoInterface m_Context = null;
        public SectionsPagerAdapter(FragmentManager fm, List<ictFileInfo> a_FileInfo, ictPlayListView.OnGetMediaInfoInterface a_Context) {
            super(fm); m_FileInfo = a_FileInfo;
            m_Context = a_Context;
        }

        void setFileInfo( List<ictFileInfo> a_FileInfo )
        {
            m_FileInfo = a_FileInfo;
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            switch(position) {
                case 0:
                    PlayListFragment playListFrag = PlayListFragment.newInstance(1, m_FileInfo);
                    return playListFrag;
                case 1: {
                    VoiceFragment fragment = VoiceFragment.newInstance("test1", "test2");
                    populateVoiceFragment(fragment);
                    return fragment;
                }
                default:
                    return ictPlayListView.newInstance( m_FileInfo, m_Context );
            }
        }

        @Override
        public int getCount() {
            // Show 3 total pages.
            return 3;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return getResources().getString(R.string.section_songlist);
                case 1:
                    return getResources().getString(R.string.section_voice);
                case 2:
                    return getResources().getString(R.string.section_playlist);
            }
            return null;
        }
    }

    public void onListFragmentInteraction(ictFileInfo item, int pos)
    {
        m_dwFileInfoIndex = item.getIndex();
        if(m_AudioInfo != null)
        {
            for(int i = 0; i < m_AudioInfo.size(); i++) {
                m_AudioInfo.get(i).m_bVisible = !(m_AudioInfo.get(i).getIndex() == item.getIndex());
            }
        }
        try {
            ictPlayListView playList = GetPlaylistViewFrag();
            PlayListFragment songList = GetAudio();
            if ( playList != null && playList.isVisible())
                playList.setSong(item);
            else {
                if (songList != null)
                    songList.setSong(item);
            }
        }catch(Exception e){
            Log.e("IAN_", e.toString());
        }
    }

    public void playSong( String a_sName )
    {
        int dwTrackPos = 0;
        for( ictFileInfo fileIn : m_AudioInfo)
        {
            if(fileIn.GetFileName() != null && fileIn.GetFileName().contains(a_sName)) {
                PlayListFragment playList = GetAudio();
                if(playList != null)
                {
                    SendSongList(m_AudioInfo, dwTrackPos );
                    playList.setSong(fileIn);
                    break;
                }
            }
            dwTrackPos++;
        }
    }
    protected boolean requestAudioPermission()
    {

        PermissionMan.PermissionData [] permissions = {
                new PermissionMan.PermissionData(Manifest.permission.RECORD_AUDIO, getString(R.string.PermissionRecordAudio))};
        return m_PermissionMan.HasPermissions(this, permissions, PermissionMan.PERMISSIONS_ALL);
    }

    public void onRequestPermissionsResult( int requestCode, String [] permissions, int [] grantResults)
    {
        this.m_PermissionMan.onRequestPermissionsResult( requestCode, permissions, grantResults);
    }

    protected void PopulateFragmentData(Context context)
    {
        MainActivity activity = (MainActivity)context;
        MediaFinder.parseAllAudio(activity, activity.m_AudioInfo);
        mSectionsPagerAdapter.setFileInfo( activity.m_AudioInfo);
        try {
            RePopulateSongList();
            populateVoiceFragment(GetVoice());
        }catch(Exception e){
            Log.e("IAN_",e.getMessage());
        }
    }

    protected void Query( String string, Context context)
    {
        MainActivity activity = (MainActivity)context;
        try
        {
            PlayListFragment songList = GetAudio();
            ictPlayListView playlist = GetPlaylistViewFrag();
            if( songList != null || playlist != null)
            {
                if( string == null || string.length() == 0 )
                {
                    if(songList != null && songList.isVisible())
                        RePopulateSongList();
                    else if(playlist != null && playlist.isVisible())
                        RePopulateSongList();
                    return;
                }
                ArrayList<ictFileInfo> found = new ArrayList<ictFileInfo>();
                if(songList != null && songList.isVisible()) {
                    for (ictFileInfo info : m_AudioInfo) {
                        if (info.GetFileName().toLowerCase(Locale.getDefault()).contains(string.toLowerCase(Locale.getDefault()))) {
                            found.add(info);
                        }
                    }
                }
                else if(playlist != null && playlist.isVisible())
                {
                    List<ictFileInfo> playlistInfo = getPlaylist();
                    for(ictFileInfo info : playlistInfo)
                        if(info.GetFileName().toLowerCase(Locale.getDefault()).contains(string.toLowerCase(Locale.getDefault())))
                            found.add(info);
                }
                if(!found.isEmpty()) {
                    if(songList != null && songList.isVisible())
                        songList.populateRecylerAdapter(found);
                    else if (playlist != null && playlist.isVisible())
                    {
                        playlist.updatePlaylist(found);
                    }
                }
            }

        }catch(Exception e)
        {
            Log.i("IAN_", e.toString());
        }
    }

    public void reInitData( VoiceFragment voice )
    {
        populateVoiceFragment(voice);
    }

    class PermissionEvent implements PermissionMan.OnPermissionEvent
    {
        Context m_pContext = null;
        PermissionEvent ( Context a_pContext)
        {
            m_pContext = a_pContext;
        }

        public void OnPermissionReceive( String [] dwPermissionType, int [] a_dwGranted  )
        {
            PopulateFragmentData(m_pContext);
        }
    }

    void populateVoiceFragment(VoiceFragment voiceFragment)
    {
        if ( voiceFragment != null )
        {
            ArrayList<String> fileNames = new ArrayList<String>();
            for( ictFileInfo fileInfo : m_AudioInfo )
            {
                fileNames.add(fileInfo.GetFileName());
            }
            if ( fileNames.size() > 0 )
                voiceFragment.SetFilesToWatch(fileNames);
        }
    }
    class AudioPermissionEvent implements PermissionMan.OnPermissionEvent
    {
        Context m_pContext = null;

        AudioPermissionEvent ( Context a_pContext)
        {
            m_pContext = a_pContext;
        }
        public void OnPermissionReceive( String [] dwPermissionType, int [] a_dwGranted )
        {
        }
    }

    class AllPermissionsEvent implements PermissionMan.OnPermissionEvent
    {
        Context m_pContext = null;
        AllPermissionsEvent ( Context a_pContext)
        {
            m_pContext = a_pContext;
        }

        public void OnPermissionReceive( String [] dwPermissionType, int [] a_dwGranted )
        {
            boolean bStartService = false;
            for ( int i = 0; i < a_dwGranted.length; i++) {
                if(a_dwGranted[i] == PackageManager.PERMISSION_GRANTED)
                {
                    if(dwPermissionType[i].equals(Manifest.permission.READ_EXTERNAL_STORAGE)) {
                        PopulateFragmentData(m_pContext);
                    }else if (dwPermissionType[i].equals(Manifest.permission.RECORD_AUDIO)){
                        VoiceFragment voiceFrag = ((MainActivity)m_pContext).GetVoice();
                        if(voiceFrag != null)
                            voiceFrag.UpdateUI(true);

                    }
                }
                if(dwPermissionType[i].equals(Manifest.permission.READ_EXTERNAL_STORAGE))
                    bStartService = true;
            }
            if(bStartService)
            {
                if(!PermissionMan.isMyServiceRunning((android.app.Activity)m_pContext, ictSndListenerService.class))
                {
                    Intent startSndListenerServiceInt = new Intent(m_pContext, ictSndListenerService.class);
                    startService(startSndListenerServiceInt);
                    doBindService();
                }
            }
        }
    }

    PlayListFragment GetAudio()
    {
        try {
            List<Fragment> fragments = getSupportFragmentManager().getFragments();
            if ( fragments != null) {
                for (Fragment fragment : fragments) {
                    if (fragment instanceof PlayListFragment) {
                        return (PlayListFragment) fragment;
                    }
                }
            }
        }catch(ClassCastException e)
        {
            throw new ClassCastException( e.toString() + " Can't find PlayListFragment");
        }
        return null;
    }

    VoiceFragment GetVoice()
    {
        try
        {
            List<Fragment> fragments = getSupportFragmentManager().getFragments();
            for( Fragment fragment : fragments ) {
                if (fragment instanceof VoiceFragment)
                    return (VoiceFragment) fragment;
            }
        }catch(ClassCastException e)
        {
            throw new ClassCastException( e.toString() + "Can't find VoiceFragment");
        }
        return null;
    }

    ictPlayListView GetPlaylistViewFrag()
    {
        try
        {
            List<Fragment> fragments = getSupportFragmentManager().getFragments();
            for( Fragment fragment : fragments ) {
                if (fragment instanceof ictPlayListView)
                    return (ictPlayListView) fragment;
            }
        }catch(ClassCastException e)
        {
            throw new ClassCastException( e.toString() + "Can't find ictPlayListView");
        }
        return null;
    }

    public void onRequestPause()
    {
        PlayListFragment pAudio = GetAudio();
        pAudio.pauseOnDifferentView();
    }

    public void onFragmentVisibility( boolean a_bVisible )
    {
        if(!a_bVisible)
        {
            onStopListeningRequest();
        }
    }

    public void onStartListeningRequest()
    {
        if(m_bIsBoundToSoundService) {
            if (m_Messenger != null && m_Service != null) {
                try {
                    Message msg = Message.obtain(null, ictSndListenerService.MSG_START_LISTENING);
                    msg.replyTo = m_Messenger;
                    m_Service.send(msg);
                }catch(RemoteException e)
                {

                }
            }
        }

    }

    public void onStopListeningRequest()
    {
        if(m_bIsBoundToSoundService) {
            if (m_Messenger != null && m_Service != null) {
                try {
                    Message msg = Message.obtain(null, ictSndListenerService.MSG_STOP_LISTENING);
                    msg.replyTo = m_Messenger;
                    m_Service.send(msg);
                }catch(RemoteException e)
                {

                }
            }
        }
    }

    public boolean onPermissionCheck()
    {
        return requestAudioPermission();
    }

    public boolean IsPlaying()
    {
        PlayListFragment audio = GetAudio();
        if( audio != null)
        {
            if(audio.isPlaying())
            {
                audio.pause();
                return ( true );
            }
        }
        return false;
    }

    public int getCurrentPosition()
    {
        if(m_bIsBoundToSoundService && m_Service != null && ictSndListenerService.s_SndListenerService != null) {
            return ictSndListenerService.s_SndListenerService.getCurrentPosition();
        }
        return 0;

    }
    public int getDuration()
    {
        if(m_bIsBoundToSoundService && m_Service != null && ictSndListenerService.s_SndListenerService != null) {
            return ictSndListenerService.s_SndListenerService.getDuration();
        }
        return 0;
    }
    public boolean isPlaying()
    {
        if(m_bIsBoundToSoundService && m_Service != null && ictSndListenerService.s_SndListenerService != null) {
            return ictSndListenerService.s_SndListenerService.isPlaying();
        }
        return false;
    }

    public List<ictFileInfo> GetServiceMediaList()
    {
        if(m_bIsBoundToSoundService && m_Service != null && ictSndListenerService.s_SndListenerService != null ) {
            return ictSndListenerService.s_SndListenerService.GetMediaList();
        }
        return null;
    }

    public int GetServiceCurrentTrackPos()
    {
        if(m_bIsBoundToSoundService && m_Service != null && ictSndListenerService.s_SndListenerService != null) {
            return ictSndListenerService.s_SndListenerService.GetCurrentTrackPos();
        }
        return 0;
    }

    public int GetLoopType()
    {
        if(m_bIsBoundToSoundService && m_Service != null && ictSndListenerService.s_SndListenerService != null) {
            return ictSndListenerService.s_SndListenerService.GetLoopType();
        }
        return m_Preferences.m_dwLoopType;
    }

    public void pause()
    {
        if(m_bIsBoundToSoundService && m_Service != null && ictSndListenerService.s_SndListenerService != null)
        {
            if(InitPopulateAudio(false)) {
                ictSndListenerService.s_SndListenerService.pause();
            }
        }
    }

    public void seekTo(int pos)
    {
        if(m_bIsBoundToSoundService && m_Service != null && ictSndListenerService.s_SndListenerService != null)
        {
            if(InitPopulateAudio(false)) {
                ictSndListenerService.s_SndListenerService.seekTo(pos);
            }
        }
    }
    public void start()
    {
        PlayerUIGeneric( ictSndListenerService.MSG_SET_SONG_START, true );
    }

    public boolean InitPopulateAudio( boolean bIfNotInitSendList )
    {
        if(!ictSndListenerService.s_SndListenerService.hasCurrentTrack()) {

            Log.e("_IANE","Don't have currentTack");
            if( bIfNotInitSendList ) {
                if (m_AudioInfo.size() > 0) {
                    SendSongList(m_AudioInfo, 0);
                    return true;
                }
            }
            return false;
        }
        return true;
    }

    public int getAudioSessionId()
    {
        if(m_bIsBoundToSoundService && m_Service != null && ictSndListenerService.s_SndListenerService != null)
        {
            return ictSndListenerService.s_SndListenerService.getAudioSessionId();
        }
        return this.hashCode();
    }

    public void setSongIndex(int index)
    {
        if(m_bIsBoundToSoundService && m_Service != null && ictSndListenerService.s_SndListenerService != null)
        {
            Log.i("IAN_", "Set song index = " + index);
            //ictSndListenerService.s_SndListenerService.setSongIndex(index);
            try {
                if(InitPopulateAudio(false)) {
                    Message msgToServ = Message.obtain(null, ictSndListenerService.MSG_SET_SONG_INDEX);
                    msgToServ.arg1 = index;
                    m_Service.send(msgToServ);
                }

            }
            catch(RemoteException e)
            {
                Log.e("IAN_","Can't send " + e.toString());
            }
        }
    }

    public void playNext()
    {
        PlayerUIGeneric( ictSndListenerService.MSG_SET_NEXT_TRACK, false );
    }

    public void PlayerUIGeneric( int dwMessage, boolean sendPlayListIfNone )
    {
        if(m_bIsBoundToSoundService && m_Service != null && ictSndListenerService.s_SndListenerService != null)
        {
            //ictSndListenerService.s_SndListenerService.playPrev();
            try {
                if(InitPopulateAudio(sendPlayListIfNone)) {
                    Message msgToServ = Message.obtain(null, dwMessage);
                    m_Service.send(msgToServ);
                }

            }
            catch(RemoteException e)
            {}
        }
    }

    public void playPrev()
    {
        PlayerUIGeneric( ictSndListenerService.MSG_SET_PREV_TRACK, false );
    }

    public void SetLoopType( int a_dwLoopType )
    {
        if(m_bIsBoundToSoundService && m_Service != null && ictSndListenerService.s_SndListenerService != null)
        {
            //ictSndListenerService.s_SndListenerService.playPrev();
            try {
                if(InitPopulateAudio(false)) {
                    Message msgToServ = Message.obtain(null, ictSndListenerService.MSG_SET_LOOP_TYPE);
                    msgToServ.arg1 = a_dwLoopType;
                    m_Preferences.m_dwLoopType = a_dwLoopType;
                    m_Service.send(msgToServ);
                }

            }
            catch(RemoteException e)
            {}
        }
    }

    public void onPrepare( int a_dwAudioSessionId, int a_dwSongIndex )
    {
        PlayListFragment frag = GetAudio();
        ictPlayListView playlist = GetPlaylistViewFrag();
        if(playlist != null && playlist.isVisible()) {
            playlist.onPrepare(a_dwAudioSessionId, a_dwSongIndex);
        }
        else {
            if (frag != null ) {
                frag.onPrepare(a_dwAudioSessionId, a_dwSongIndex);
            }
        }

        if(m_Visualizer == null)
        {
            m_Visualizer = new ictAudioVisualizerProducer(this);
        }

        try {
            m_Visualizer.Reset(a_dwAudioSessionId);
            m_Visualizer.SetEnable(true);
        }catch(IllegalStateException e)
        {
            Log.e("IANE_", e.toString());
        }
        m_dwFileInfoIndex = a_dwSongIndex;
        UpdatePlayListUI();
    }

    public void onCompletion( int a_dwAudioSessionId)
    {
        PlayListFragment frag = GetAudio();
        if(frag != null) {
            frag.onCompletion(a_dwAudioSessionId);
        }

        if(m_Visualizer != null)
            m_Visualizer.SetEnable( false );
    }
    public void SetTTS( boolean a_bTTS )
    {
        m_Preferences.m_bTTS = a_bTTS;
        if(m_bIsBoundToSoundService && m_Service != null)
        {
            try {
                Message msg = Message.obtain(null, ictSndListenerService.MSG_UPDATE_PREFERENCES);
                Bundle bundle = new Bundle();
                bundle.putParcelable(ictPreferences.PREF_NAME, m_Preferences);
                msg.setData(bundle);
                m_Service.send(msg);
            }catch(RemoteException e)
            {}
        }

    }

    public int GetSongPos() { return m_dwFileInfoIndex; }

    protected void UpdatePlayListUI()
    {
        PlayListFragment frag = GetAudio();
        if(frag != null && frag.getView() != null ) {
            RecyclerView recyclerView = (RecyclerView) frag.getView().findViewById(R.id.list);
            if (recyclerView != null && m_AudioInfo != null) {
                RecyclerView.Adapter adapter = recyclerView.getAdapter();
                if(adapter instanceof MyPlayListRecyclerViewAdapter) {
                    MyPlayListRecyclerViewAdapter recyclerAdapter = (MyPlayListRecyclerViewAdapter) adapter;
                    recyclerAdapter.notifyDataSetChanged();
                }
            }
        }
        ictPlayListView playList = GetPlaylistViewFrag();
        if(playList != null)
            if(playList.m_RecyclerViewAdapter != null)
                playList.m_RecyclerViewAdapter.notifyDataSetChanged();
    }

    public void CreatNewPlaylist( String sPlaylist, String sDescription, ictFileInfo fileInfo )
    {
        m_Playlists.CreatePlaylist( sPlaylist, sDescription, fileInfo );
    }

    public void GetPlaylistEntries( List<String> a_PlayEntries)
    {
        m_Playlists.GetPlaylistEntries( a_PlayEntries );
    }

    public String GetPlaylistLastSelected()
    {
        return m_Playlists.GetPlaylistLastSelected();
    }

    public void AddToPlaylist(String playlistName, ictFileInfo fileInfo )
    {
        m_Playlists.AddToPlaylist( playlistName, fileInfo.GetFileName(), fileInfo.getIndex() );
    }

    public boolean MoveItemInPlayList(int a_dwFromPos, int a_dwToPos)
    {
        return m_Playlists.MoveItemInPlayList(a_dwFromPos, a_dwToPos);
    }

    public boolean UndoLastDeleted()
    {
        boolean bUndo = m_Playlists.UndoLastDeleted();
        if(bUndo)
        {
            if(m_bIsBoundToSoundService && m_Service != null && ictSndListenerService.s_SndListenerService != null)
            {
                try {
                    Message msgToServ = Message.obtain(null, ictSndListenerService.MSG_UNDELETE_MEDIA);
                    m_Service.send(msgToServ);

                }
                catch(RemoteException e)
                {}
            }
        }
        return bUndo;

    }
    public boolean DeleteMedia( int pos )
    {
        if(m_bIsBoundToSoundService && m_Service != null && ictSndListenerService.s_SndListenerService != null)
        {
            try {
                Message msgToServ = Message.obtain(null, ictSndListenerService.MSG_DELETE_MEDIA);
                msgToServ.arg1 = pos;
                m_Service.send(msgToServ);
                return true;
            }
            catch(RemoteException e)
            {}
        }
        return false;
    }

    protected void SetVisualizerDataFor( RecyclerView recyclerView, int a_dwFileInfoIndex, byte [] data )
    {
        if ( recyclerView != null && m_AudioInfo != null) {
            if (a_dwFileInfoIndex != -1 ) {
                for( int i = 0; i < recyclerView.getChildCount(); i++)
                {
                    MyPlayListRecyclerViewAdapter.ViewHolder viewHolder = (MyPlayListRecyclerViewAdapter.ViewHolder)recyclerView.getChildViewHolder(recyclerView.getChildAt(i));
                    if(viewHolder != null) {
                        if(viewHolder.m_dwFileInfoIndex == a_dwFileInfoIndex )
                        {
                            if(viewHolder.m_Visualizer != null )
                                viewHolder.m_Visualizer.setData(data);
                            //break;
                        }

                    }
                }
            }
        }
    }

    public void onDataReceived( byte [] data )
    {
        try
        {
            PlayListFragment frag = GetAudio();
            if(frag != null && frag.getView() != null )
            {
                RecyclerView recyclerView = (RecyclerView)frag.getView().findViewById(R.id.list);
                SetVisualizerDataFor(recyclerView, m_dwFileInfoIndex, data);
            }
            else
            {
                ictPlayListView playListView = GetPlaylistViewFrag();
                if(playListView != null && playListView.getView() != null)
                {
                    RecyclerView recyclerView = (RecyclerView)playListView.getView().findViewById(R.id.playlist_view);
                    SetVisualizerDataFor(recyclerView, m_dwFileInfoIndex, data);
                }
            }
        }catch(ClassCastException e)
        {

        }

    }

    public List<ictFileInfo> getPlaylist()
    {
        ictPlaylistData pCurrPlaylistData = m_Playlists.getCurrentPlaylist();
        if(pCurrPlaylistData != null)
        {
            List<ictFileInfo> pCurrPlaylist =  new ArrayList<ictFileInfo>();
            for( ictPlaylistData.ictPlaylistEntry title : pCurrPlaylistData.m_MediaTitle)
            {
                for(ictFileInfo media : m_AudioInfo)
                {
                    if(media.getIndex() == title.m_dwIndex)
                    {
                        pCurrPlaylist.add(media);
                    }
                }
            }
            return pCurrPlaylist;
        }
        return null;
    }

    public void GetPlaylistTitles(List<String> playlistTitles )
    {
        m_Playlists.GetPlaylistEntries(playlistTitles);
    }

    public void SelectPlaylist(String playlistName)
    {
        m_Playlists.SelectPlaylist(playlistName);
    }

    public boolean DeleteSongFromCurrPlaylist(ictFileInfo fileInfo)
    {
        return m_Playlists.DeleteSongFromCurrPlaylist(fileInfo);
    }
    public String GetSelectedPlaylist()
    {
        return m_Playlists.GetSelectedPlaylist();
    }
    public void OnPlaylistChanged( ictPlaylistData playList )
    {
        ictPlayListView pPlayListView = GetPlaylistViewFrag();
        if(pPlayListView != null)
        {
            pPlayListView.updatePlaylist(getPlaylist());
        }
    }

    public boolean DeletePlaylist( String playlistName )
    {
        return m_Playlists.DeletePlaylist( playlistName );
    }

    public Context GetContext()
    {
        return this;
    }

    public void SendSongList(List<ictFileInfo> a_ListInfo, int pos)
    {
        SendMediaToServer(a_ListInfo, pos);
    }

    /**
     * Overlay
    @Override
    //@TargetApi(Build.VERSION_CODES.M)
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult( requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE) {
            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (Settings.canDrawOverlays(this)) {
                    startService(new Intent(this, ictSndListenerService.class));
                    doBindService();
                }
            }
        }
    }
    */
}


class ictLanguageDetailsChecker2 extends BroadcastReceiver
{
    private List<String> supportedLanguages;

    private String languagePreference;


    @Override
    public void onReceive(Context context, Intent intent)
    {
        Bundle results = getResultExtras(true);
        if (results.containsKey(RecognizerIntent.EXTRA_LANGUAGE_PREFERENCE))
        {
            languagePreference =
                    results.getString(RecognizerIntent.EXTRA_LANGUAGE_PREFERENCE);
        }
        if (results.containsKey(RecognizerIntent.EXTRA_SUPPORTED_LANGUAGES))
        {
            supportedLanguages =
                    results.getStringArrayList(
                            RecognizerIntent.EXTRA_SUPPORTED_LANGUAGES);
        }
    }
}
