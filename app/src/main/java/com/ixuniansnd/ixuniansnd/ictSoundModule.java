package com.ixuniansnd.ixuniansnd;

import android.content.Context;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.Message;
import android.os.Messenger;
import android.os.Parcelable;
import android.os.RemoteException;
import android.speech.tts.TextToSpeech;
import android.util.Log;
import android.widget.SeekBar;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.HashMap;
import java.io.IOException;

import static com.ixuniansnd.ixuniansnd.PlayListFragment.s_PlayerShowDuration;
import com.ixuniansnd.ixuniansnd.ictDoubleLinkList;

/**
 * Created by trong on 5/14/17. Sound module.
 */

public class ictSoundModule implements MediaPlayer.OnPreparedListener
        , MediaPlayer.OnCompletionListener {


    public interface ictSoundModuleInterface extends Serializable
    {
        // MediaController
        int getCurrentPosition();
        int getDuration();
        boolean isPlaying();
        void pause();
        void seekTo(int pos);
        void start();
        int getAudioSessionId();
        void setSongIndex(int index);
        void onPrepare(int a_dwAudioSessionId, int a_dwSongIndex );
        void onCompletion ( int a_dwSessionId );

        void playNext();
        void playPrev();
        void SetLoopType( int a_dwLoopType );
    }

    public interface SoundModuleCallback
    {
        void onPrepare( int a_dwSessionId, int a_dwSongIndex );
        void onCompletion ( int a_dwSessionId );
        boolean shouldContinuePlaying();
    }

    Context m_Context =null;

    private volatile ictDoubleLinkList<ictFileInfo> m_AudioInfo = new ictDoubleLinkList<>();

    class ictDeleteMediaHelper
    {
        ictDeleteMediaHelper( int a_dwPos, ictDoubleLinkList<ictFileInfo>.ictNode a_Node)
        {
            m_dwPos = a_dwPos;
            m_DeletedNode = a_Node;
        }
        int m_dwPos;
        ictDoubleLinkList<ictFileInfo>.ictNode m_DeletedNode;
    }
    private volatile List<ictDeleteMediaHelper> m_DeletedNodes = new ArrayList<>();

    private TextToSpeech m_TTS = null;
    private boolean m_bCanTTS = false;
    private boolean m_bEnableTTS = true;

    private static volatile ictDoubleLinkList<ictFileInfo>.ictIterator m_CurrentTrack = null;
    private static volatile boolean m_bMediaPlayerHasInitted = false;

    private static MediaPlayer m_MediaPlayer = null;
    private SoundModuleCallback m_SoundModuleCallback = null;
    int m_dwLoopType = 0;

    ictSoundModule( Context a_Context, SoundModuleCallback a_Callback )
    {
        m_Context = a_Context;
        m_SoundModuleCallback = a_Callback;
    }

    public void populateFiles ( ArrayList<ictFileInfo> files, int songPos )
    {
        m_DeletedNodes.clear();
        m_AudioInfo.Free();
        for( ictFileInfo media : files) {
            m_AudioInfo.AddBack(media);
        }

        m_CurrentTrack = m_AudioInfo.Iterator();
        m_CurrentTrack.SetPos(songPos);
       // Log.i("IAN_", "Sent over playlist with " + m_AudioInfo.Size() + " entries pos=" + songPos);
    }

    public void Init()
    {
        if(m_MediaPlayer == null) {
            m_MediaPlayer = new MediaPlayer();
        }
        m_MediaPlayer.setOnCompletionListener(this);
        m_MediaPlayer.setOnPreparedListener(this);
        createTTS();
    }

    private void createTTS()
    {
        if(m_TTS != null)
        {
            return;
        }
        m_TTS = new TextToSpeech(m_Context,
                new TextToSpeech.OnInitListener() {
                    @Override
                    public void onInit(int status) {
                        if(status == TextToSpeech.SUCCESS)
                        {
                            Locale locale;
                            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.N)
                            {
                                locale = m_Context.getResources().getConfiguration().getLocales().get(0);
                            }
                            else {
                                locale = m_Context.getResources().getConfiguration().locale;
                            }
                            int result = m_TTS.setLanguage(locale);
                            if(result == TextToSpeech.LANG_MISSING_DATA || result == TextToSpeech.LANG_NOT_SUPPORTED)
                            {
                                Log.e("TTS","Language not supported");
                                m_bCanTTS = false;
                            }
                            else
                            {
                                m_bCanTTS = true;
                            }
                        }
                    }
                });
    }

    public void onDestroy()
    {
        if(m_TTS != null)
        {
            m_TTS.stop();
            m_TTS.shutdown();
            m_TTS = null;
        }
    }

    public void shutDown()
    {
        m_MediaPlayer.stop();
        m_MediaPlayer = null;
        onDestroy();
    }

    public int getCurrentPosition()
    {
        if(!m_bMediaPlayerHasInitted)
            return 0;
        return m_MediaPlayer.getCurrentPosition();

    }
    public int getDuration()
    {
        if(!m_bMediaPlayerHasInitted)
            return 0;
        try {
            int dwDuration = m_MediaPlayer.getDuration();
            if(dwDuration < 0) {
                Log.e("IAN_", "NegativeDuration!");
                return 0;
            }
            return dwDuration;
        }catch(Exception e)
        {
            Log.e("IAN_","getDuration error " + e.toString());
            return 0;
        }
    }
    public boolean isPlaying() { return m_MediaPlayer.isPlaying(); }

    public void onCompletion(MediaPlayer a_MediaPlayer)
    {
        if(m_SoundModuleCallback.shouldContinuePlaying()) {
            // Seeing that diff isn't 0 but 349, 234, etc...
            //int diff = Math.abs(m_MediaPlayer.getCurrentPosition() - m_MediaPlayer.getDuration());
            /*if ( diff == 0 )*/ {
                if (m_dwLoopType == 1) {
                    //setTrackIndex(m_CurrentTrack.GetPos());
                    m_MediaPlayer.seekTo(0);
                    m_MediaPlayer.start();
                    return;
                } else {
                    //Log.i("IAN_", "---- ---> diff " + diff + " isPlaying = " + m_MediaPlayer.isPlaying());
                        m_SoundModuleCallback.onCompletion(a_MediaPlayer.getAudioSessionId());
                        playNext(false);
                    return;
                }
            }
        }
        m_SoundModuleCallback.onCompletion(a_MediaPlayer.getAudioSessionId());
    }


    public void pause()
    {
        m_MediaPlayer.pause();
        if(m_TTS != null)
        {
            if(m_TTS.isSpeaking())
                m_TTS.stop();
        }
    }

    public void seekTo(int pos)
    {
        m_MediaPlayer.seekTo(pos);
    }

    public int getAudioSessionId()
    {
        return m_MediaPlayer.getAudioSessionId();
    }

    public boolean hasCurrentTrack()
    {
        return ( m_CurrentTrack != null );
    }

    public void start()
    {
        try {
            if ( !m_bMediaPlayerHasInitted ){
                if(m_CurrentTrack != null) {
                    setTrackIndex(m_CurrentTrack.GetPos());
                }
            }
            else {
                if (!isPlaying()) {
                    m_MediaPlayer.start();
                }
            }

        }catch(IllegalStateException e)
        {
            Log.e("IAN_", e.toString());
        }
    }


    private int GetTrackIndexFromFileInfo( int a_dwFileInfoIndex ) {
        if (m_AudioInfo == null)
            return 0;
        ictDoubleLinkList<ictFileInfo>.ictIterator itr = m_AudioInfo.Iterator();
        int i = 0;
        while( itr.Get() != null)
        {
            ictDoubleLinkList<ictFileInfo>.ictNode pCurNode = itr.Get();
            if(pCurNode.m_Element.getIndex() == a_dwFileInfoIndex)
                return i;
            itr.Next();
            i++;
        }
        return 0;
    }

    private void setTrackIndex( int a_dwCurrentTrack)
    {
        if(a_dwCurrentTrack < 0)
            return;
        if(a_dwCurrentTrack < m_AudioInfo.Size()){
            ictDoubleLinkList<ictFileInfo>.ictIterator itr = m_AudioInfo.Iterator();
            itr.SetPos(a_dwCurrentTrack);
            Log.i("IAN_","2b. setTrackIndex("+a_dwCurrentTrack+") = " + itr.Get().m_Element.GetFileName());
            setSong(itr.Get().m_Element, a_dwCurrentTrack);
        }
    }

    public void playNext( boolean a_bPressed )
    {
        if( m_CurrentTrack != null && m_AudioInfo != null && m_AudioInfo.Size() > 0)
        {
            ictDoubleLinkList<ictFileInfo>.ictNode pCurNode = m_CurrentTrack.NextCycle();
            if(pCurNode != null) {

                int dwCurPos = m_CurrentTrack.GetPos();
                Log.i("IAN_","1. playNext pos= " + dwCurPos);
                setTrackIndex(dwCurPos);
            }
        }
    }
    public void playPrev( boolean a_bPressed )
    {
        if( m_CurrentTrack != null  && m_AudioInfo != null && m_AudioInfo.Size() > 0)
        {
            ictDoubleLinkList<ictFileInfo>.ictNode pCurNode = m_CurrentTrack.PrevCycle();
            if( pCurNode != null) {
                int dwCurPos = m_CurrentTrack.GetPos();
                //Log.i("IAN_","1. playPrev pos= " + dwCurPos);
                setTrackIndex( dwCurPos );
            }
        }
    }

    public void setSongIndex ( int a_dwIndex )
    {
        if(m_AudioInfo == null)
        {
            return;
        }
        //Log.i("IAN_","2. setSongIndex = " + a_dwIndex + "of amount=" + m_AudioInfo.Size());
        ictDoubleLinkList<ictFileInfo>.ictIterator itr = m_AudioInfo.Iterator();
        ictDoubleLinkList<ictFileInfo>.ictNode pCurNode;
        while(( pCurNode = itr.Get() ) != null)
        {
            if(pCurNode.m_Element.getIndex() == a_dwIndex)
            {
                setSong(pCurNode.m_Element);
                break;
            }
            itr.Next();
        }
    }

    public void SetLoopType( int a_dwLoopType )
    {
        m_dwLoopType = a_dwLoopType;
    }

    protected void setSong(ictFileInfo a_FileInfo)
    {
        setSong( a_FileInfo, -1 );
    }

    protected void setSong(ictFileInfo a_FileInfo, int a_dwIndex )
    {
        //Log.i("IAN_", "3.------>Set song:  " + a_FileInfo.GetFileName());
        if(m_MediaPlayer != null)
        {
            try {
                if(m_bCanTTS && m_bEnableTTS) {
                    String utteranceId = a_FileInfo.toString().hashCode()+"";
                    if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        m_TTS.speak(a_FileInfo.GetFileName(), TextToSpeech.QUEUE_FLUSH, null, utteranceId);
                       // Log.i("IAN_","4. spoke: " + a_FileInfo.GetFileName());
                    }
                    else
                    {
                        HashMap<String, String> map = new HashMap<>();

                        map.put(TextToSpeech.Engine.KEY_PARAM_UTTERANCE_ID, utteranceId);
                        m_TTS.speak(a_FileInfo.GetFileName(), TextToSpeech.QUEUE_FLUSH, map);
                    }
                }

                m_CurrentTrack.SetPos ( a_dwIndex >= 0 ? a_dwIndex : GetTrackIndexFromFileInfo(a_FileInfo.getIndex()) );

                Log.i("IAN_", "5. Current Track index:" + m_CurrentTrack.GetPos() + " " + a_FileInfo.GetFileName());
                m_MediaPlayer.reset();
                m_MediaPlayer.setOnErrorListener( new MediaPlayer.OnErrorListener()
                {
                    @Override
                    public boolean onError(MediaPlayer mp, int what, int extra)
                    {
                        Log.e("IAN_", "MPError what=" + Integer.toString(what) + " extra=" + Integer.toString(extra));
                        return false;
                    }
                });
                m_MediaPlayer.setDataSource(a_FileInfo.GetFullPath());
                m_MediaPlayer.prepareAsync();

            }catch(IOException e)
            {
                Log.e("IAN_", e.toString());
            }
        }
    }

    public void onPrepared(MediaPlayer player)
    {
        try {
            //ICT TODO: send onPrepareMessage
            if (m_SoundModuleCallback != null) {
                int dwPos = m_CurrentTrack.GetPos();
                if (dwPos >= 0 && dwPos < m_AudioInfo.Size()) {
                    ictDoubleLinkList<ictFileInfo>.ictNode pCurNode = m_CurrentTrack.Get();
                    m_SoundModuleCallback.onPrepare(player.getAudioSessionId(), pCurNode.m_Element.getIndex());
                }
            }
            m_MediaPlayer.start();
            m_bMediaPlayerHasInitted = true;
        }catch(Exception e)
        {
            Log.e("IAN_",e.toString());
        }

    }

    public void setEnableTTS(boolean a_bTTS)
    {
        m_bEnableTTS = a_bTTS;
    }

    public List<ictFileInfo> GetMediaList() {
        if( m_AudioInfo != null) {
            List<ictFileInfo> media = new ArrayList<>();
            ictDoubleLinkList<ictFileInfo>.ictIterator itr = m_AudioInfo.Iterator();
            ictDoubleLinkList<ictFileInfo>.ictNode pCurNode;
            while ((pCurNode = itr.Get()) != null) {
                media.add(pCurNode.m_Element);
                itr.Next();
            }
            return media;
        }
        return null;
    }
    int GetCurrentTrackPos() {
        if (m_CurrentTrack == null)
                return 0;
        return m_CurrentTrack.GetPos();
    }
    int GetLoopType() { return m_dwLoopType; }

    boolean DeleteMedia( int pos )
    {
        if(m_AudioInfo != null)
        {
            ictDoubleLinkList<ictFileInfo>.ictIterator itr = m_AudioInfo.Iterator();
            ictDoubleLinkList<ictFileInfo>.ictNode pToDelete = itr.SetPos( pos );
            if( pToDelete != null )
            {
                m_DeletedNodes.clear();

                m_AudioInfo.Delete( pToDelete );
                m_DeletedNodes.add(new ictDeleteMediaHelper( pos, pToDelete) );
                return true;
            }
        }
        return false;
    }

    boolean UnDeleteMedia()
    {
        if( !m_DeletedNodes.isEmpty() )
        {
            ictDeleteMediaHelper pUndel = m_DeletedNodes.get(0);
            m_DeletedNodes.clear();
            ictDoubleLinkList<ictFileInfo>.ictNode pPrevNode = null;
            if(pUndel.m_DeletedNode.m_Prev != null)
                pPrevNode = pUndel.m_DeletedNode.m_Prev;
            m_AudioInfo.InsertAfter(pPrevNode, pUndel.m_DeletedNode);

            return true;
        }
        return false;
    }
}
