package com.ixuniansnd.ixuniansnd;

import android.content.Context;
import android.content.Intent;
import android.speech.RecognitionListener;
import android.speech.RecognizerIntent;
import android.speech.SpeechRecognizer;
import android.util.Log;
import android.os.Bundle;
import java.util.List;

import com.ixuniansnd.ixuniansnd.ictSpeechActivator;
import com.ixuniansnd.ixuniansnd.WordList;
import com.ixuniansnd.ixuniansnd.SoundsLikeWordMatcher;
import com.ixuniansnd.ixuniansnd.SpeechActivationListener;

/**
 * Created by trong on 4/24/17.
 */

public class ictSpeechRecognitionActivator implements ictSpeechActivator, RecognitionListener {

    private final String TAG = "IAN_";

    private Context m_Context;
    private SpeechRecognizer m_SpeechRecognizer = null;
    private SoundsLikeWordMatcher m_Matcher = null;

    private SpeechActivationListener m_ResultListener;

    ictSpeechRecognitionActivator( Context a_Context, SpeechActivationListener a_ResultListener, String... targetWords )
    {
        m_Context = a_Context;
        m_Matcher = new SoundsLikeWordMatcher(targetWords);
        m_ResultListener = a_ResultListener;
    }

    public void detectActivation()
    {
        recognizeSpeechDirectly();
    }

    private void recognizeSpeechDirectly()
    {
        Intent recognizerIntent =
                new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        recognizerIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
                RecognizerIntent.LANGUAGE_MODEL_WEB_SEARCH);
        // accept partial results if they come
        recognizerIntent.putExtra(RecognizerIntent.EXTRA_PARTIAL_RESULTS, true);
        //need to have a calling package for it to work
        if (!recognizerIntent.hasExtra(RecognizerIntent.EXTRA_CALLING_PACKAGE))
        {
            recognizerIntent.putExtra(RecognizerIntent.EXTRA_CALLING_PACKAGE, m_Context.getPackageName());
        }

        SpeechRecognizer recognizer = getSpeechRecognizer();
        recognizer.setRecognitionListener(this);
        recognizer.startListening(recognizerIntent);
        Log.d(TAG,"recognizer.startListening");
    }

    public void stop()
    {
        Log.e(TAG,"ictSpeechRecognitionActivator.stop()");
        if (getSpeechRecognizer() != null)
        {
            getSpeechRecognizer().stopListening();
            getSpeechRecognizer().cancel();
            getSpeechRecognizer().destroy();
        }
    }

    public static String diagnoseErrorCode(int errorCode)
    {
        String message;
        switch (errorCode)
        {
            case SpeechRecognizer.ERROR_AUDIO:
                message = "Audio recording error";
                break;
            case SpeechRecognizer.ERROR_CLIENT:
                message = "Client side error";
                break;
            case SpeechRecognizer.ERROR_INSUFFICIENT_PERMISSIONS:
                message = "Insufficient permissions";
                break;
            case SpeechRecognizer.ERROR_NETWORK:
                message = "Network error";
                break;
            case SpeechRecognizer.ERROR_NETWORK_TIMEOUT:
                message = "Network timeout";
                break;
            case SpeechRecognizer.ERROR_NO_MATCH:
                message = "No match";
                break;
            case SpeechRecognizer.ERROR_RECOGNIZER_BUSY:
                message = "RecognitionService busy";
                break;
            case SpeechRecognizer.ERROR_SERVER:
                message = "error from server";
                break;
            case SpeechRecognizer.ERROR_SPEECH_TIMEOUT:
                message = "No speech input";
                break;
            default:
                message = "Didn't understand, please try again.";
                break;
        }
        return message;
    }

    private void receiveResults(Bundle results)
    {
        if ((results != null)
                && results.containsKey(SpeechRecognizer.RESULTS_RECOGNITION))
        {
            List<String> heard =
                    results.getStringArrayList(SpeechRecognizer.RESULTS_RECOGNITION);
            float[] scores =
                    results.getFloatArray(SpeechRecognizer.CONFIDENCE_SCORES);
            receiveWhatWasHeard(heard, scores);
        }
        else
        {
            Log.d(TAG, "no results");
        }
    }

    private void receiveWhatWasHeard(List<String> heard, float[] scores)
    {
        boolean heardTargetWord = false;
        // find the target word
        for (String possible : heard)
        {
            WordList wordList = new WordList(possible);
            if (m_Matcher.isIn(wordList.getWords()))
            {
                Log.d(TAG, "HEARD IT!");
                heardTargetWord = true;
                break;
            }
        }

        if (heardTargetWord)
        {
            stop();
            m_ResultListener.activated(true);
        }
        else
        {
            // keep going
            recognizeSpeechDirectly();
        }
    }

    @Override
    public void onError(int errorCode)
    {
        if ((errorCode == SpeechRecognizer.ERROR_NO_MATCH)
                || (errorCode == SpeechRecognizer.ERROR_SPEECH_TIMEOUT))
        {
            Log.d(TAG, "didn't recognize anything");
            // keep going
            recognizeSpeechDirectly();
        }
        else
        {
            Log.d(TAG,
                    "FAILED "
                            +
                            diagnoseErrorCode(errorCode));
        }
    }

    private SpeechRecognizer getSpeechRecognizer()
    {
        if (m_SpeechRecognizer == null)
        {
            m_SpeechRecognizer = SpeechRecognizer.createSpeechRecognizer(m_Context);
        }
        return m_SpeechRecognizer;
    }

    @Override
    public void onResults(Bundle results)
    {
        Log.d(TAG, "full results");
        receiveResults(results);
    }

    @Override
    public void onPartialResults(Bundle partialResults)
    {
        Log.d(TAG, "partial results");
        receiveResults(partialResults);
    }

    // other unused methods from RecognitionListener...

    @Override
    public void onReadyForSpeech(Bundle params)
    {
        Log.d(TAG, "ready for speech " + params);
    }

    @Override
    public void onEndOfSpeech()
    {
        Log.d(TAG,"onEndOfSpeech");
    }

    /**
     * @see android.speech.RecognitionListener#onBeginningOfSpeech()
     */
    @Override
    public void onBeginningOfSpeech()
    {
        Log.d(TAG,"onBeginningofSpeech");
    }

    @Override
    public void onBufferReceived(byte[] buffer)
    {
        Log.d(TAG,"onBufferReceived");
    }

    @Override
    public void onRmsChanged(float rmsdB)
    {
    }

    @Override
    public void onEvent(int eventType, Bundle params)
    {
        Log.d(TAG,"onEvent");
    }

}
