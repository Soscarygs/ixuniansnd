package com.ixuniansnd.ixuniansnd;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by trong on 2/14/17.
 */

public class ictFileInfo implements Parcelable {
    private String m_sFullPath;
    private String m_sFileName;
    private int m_dwIndex = 0;
    public boolean m_bVisible = true;

    ictFileInfo()
    {}

    public void SetFullPath( String a_sFullPath) { m_sFullPath = a_sFullPath; }
    public String GetFullPath() { return m_sFullPath; }
    public void SetFileName( String a_sFileName ) { m_sFileName = a_sFileName; }
    public String GetFileName() { return m_sFileName; }

    public int describeContents()
    {
        return 0;
    }
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(m_sFullPath);
        dest.writeString(m_sFileName);
        dest.writeInt(m_dwIndex);
        dest.writeInt( m_bVisible ? 1 : 0 );
    }

    // Creator
    public static final Parcelable.Creator<ictFileInfo> CREATOR = new Parcelable.Creator<ictFileInfo>()
    {
        public ictFileInfo createFromParcel(Parcel in)
        {
            return new ictFileInfo(in);
        }
        public ictFileInfo[] newArray( int size)
        {
            return new ictFileInfo[size];
        }
    };

    public ictFileInfo(Parcel in)
    {
        m_sFullPath = in.readString();
        m_sFileName = in.readString();
        m_dwIndex = in.readInt();
        m_bVisible = in.readInt() > 0;
    }

    public String toString(){
        return m_sFileName;
    }

    public void setIndex(int a_dwIndex) { m_dwIndex = a_dwIndex; }
    public final int getIndex() { return m_dwIndex; }
}
