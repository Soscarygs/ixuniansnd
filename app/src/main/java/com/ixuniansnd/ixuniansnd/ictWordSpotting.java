package com.ixuniansnd.ixuniansnd;

/**
 * Created by trong on 5/7/17.
 */

import android.util.Log;

import com.ixuniansnd.ixuniansnd.WordMatcher;
import com.ixuniansnd.ixuniansnd.SoundsLikeWordMatcher;

import java.text.Normalizer;
import java.util.ArrayList;

public class ictWordSpotting {
    SoundsLikeWordMatcher m_Sentence = null;
    int m_dwCountSimilar = 0;
    String m_sOriginal = null;
    String [] m_Tokens = null;

    String [] Clean( String sOriginal)
    {
        //sOriginal = "Paul~Mau#riat=&this_that,yeu, toi hai[la]hom-can(red).mp3";
        Normalizer.normalize(sOriginal, Normalizer.Form.NFD).replaceAll("[^\\p{ASCII}]", "");
        return sOriginal.split("[\\s,_~#&=\\?\\[\\]\\.\\-\\(\\)]+");
    }

    ictWordSpotting( String sentence )
    {

        //sentence="hello\u00ea\u00f1-world?How_[Are]you~doing this#190 Johnny&Santos";
        m_sOriginal = sentence;
        String sOriginal = sentence;
        String [] tokens = Clean(sOriginal);
        m_Tokens = tokens;
        m_Sentence = new SoundsLikeWordMatcher(tokens);
    }
    void Reset()
    {
        m_dwCountSimilar = 0;
    }
    void Process(ArrayList<String> input )
    {
        for( String token : input )
        {
            String [] tokens = Clean(token);
            for( int i = 0; i < tokens.length; i++) {
                if (m_Sentence.isIn(tokens[i])) {
                    m_dwCountSimilar++;
                }
            }
        }
    }
    void Process(String a_sToken )
    {
        String [] tokens = Clean(a_sToken);
        for( int i = 0; i < tokens.length; i++ ) {
            if (m_Sentence.isIn(tokens[i])) {
                m_dwCountSimilar++;
            }
        }
    }

    String getOriginalString() { return m_sOriginal; }

    final int getCount() { return m_dwCountSimilar; }
    final int getToken()
    {
        if ( m_Tokens == null)
            return 0;
        return m_Tokens.length;
    }
}
