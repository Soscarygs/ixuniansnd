package com.ixuniansnd.ixuniansnd;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Parcel;
import android.support.v4.app.ActivityCompat;
import android.os.Parcelable;

/**
 * Created by trong on 5/14/17.
 */

public class ictPreferences implements Parcelable {
    public static final String PREF_NAME = "IxunianSnd";
    boolean m_bTTS = true;
    int m_dwLoopType = 0;
    Context m_Context = null;

    public ictPreferences( Context context )
    {
        m_Context = context;
    }
    public void RestorePreferences()
    {
        SharedPreferences preferences = m_Context.getSharedPreferences(PREF_NAME, 0);
        m_bTTS = preferences.getBoolean("TTS", true);
        m_dwLoopType = preferences.getInt("LoopType", 0);
    }

    public void WritePreferences()
    {
        SharedPreferences preferences = m_Context.getSharedPreferences(PREF_NAME, 0);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean("TTS", m_bTTS);
        editor.putInt("LoopType", m_dwLoopType);
        editor.commit();
    }

    public static final Parcelable.Creator<ictPreferences> CREATOR =
            new Parcelable.Creator<ictPreferences>()
            {
                public ictPreferences createFromParcel(Parcel source)
                {
                    return new ictPreferences(source);
                }
                public ictPreferences[] newArray(int size)
                {
                    return new ictPreferences[size];
                }
            };
    public void writeToParcel(Parcel out, int flags)
    {
        byte bTTS = (byte) (m_bTTS ? 1 : 0);
        out.writeByte( bTTS );
        out.writeInt(m_dwLoopType);
    }
    public int describeContents()
    {
        return 0;
    }
    private ictPreferences(Parcel parcel)
    {
        m_bTTS = parcel.readByte() != 0;
        m_dwLoopType = parcel.readInt();
    }

}
