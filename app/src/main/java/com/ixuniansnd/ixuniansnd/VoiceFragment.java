package com.ixuniansnd.ixuniansnd;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;


import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import com.ixuniansnd.ixuniansnd.WordList;
import com.ixuniansnd.ixuniansnd.SoundsLikeWordMatcher;
import com.ixuniansnd.ixuniansnd.ictWordSpotting;

/**
 * Use the {@link VoiceFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class VoiceFragment extends Fragment implements View.OnClickListener {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    // The request code must be 0 or greater.
     // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;


    private Button m_btnStartListening = null;
    private Button m_btnStopListening = null;
    private TextView m_voiceStatus = null;

    OnRequestPauseListener m_RequestPauseListener = null;
    ArrayList<ictWordSpotting> m_Files = null;

    public VoiceFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment VoiceFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static VoiceFragment newInstance(String param1, String param2) {
        VoiceFragment fragment = new VoiceFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_voice, container, false);
        m_btnStartListening = (Button)view.findViewById(R.id.btn_voice_start_listening);
        m_btnStartListening.setOnClickListener(this);
        m_btnStopListening = (Button)view.findViewById(R.id.btn_voice_stop_listening);
        m_btnStopListening.setOnClickListener(this);
        m_voiceStatus = (TextView)view.findViewById(R.id.voice_command_status);
        m_voiceStatus.setTextSize(10);
        m_voiceStatus.setText(getString(R.string.voice_command_status));
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        m_RequestPauseListener.reInitData(this);
    }

    public void UpdateUI ( boolean a_bEnabled )
    {
        m_btnStartListening.setEnabled(a_bEnabled);
        m_btnStopListening.setEnabled(a_bEnabled);
    }

    public void onClick(View view)
    {
        switch(view.getId())
        {
            case R.id.btn_voice_start_listening:
                if(m_RequestPauseListener.onPermissionCheck()) {
                    m_RequestPauseListener.IsPlaying();
                    m_RequestPauseListener.onStartListeningRequest();
                    UpdateUI ( true );
                }
                else
                    UpdateUI( false );
                Log.i("IAN_", "Start Listenening");
                break;
            case R.id.btn_voice_stop_listening:
                if(m_RequestPauseListener.onPermissionCheck()) {
                    m_RequestPauseListener.onStopListeningRequest();
                    UpdateUI ( true );
                }
                else
                    UpdateUI ( false );
                Log.i("IAN_", "Stop Listening");
                break;
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        try{
            m_RequestPauseListener = (OnRequestPauseListener)context;
        }catch(ClassCastException e)
        {
            throw new ClassCastException(context.toString() + " must implement the OnRequestPauseListener interface");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        m_RequestPauseListener = null;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (m_RequestPauseListener != null) {
            if (isVisibleToUser)
                m_RequestPauseListener.onRequestPause();
            m_RequestPauseListener.onFragmentVisibility(isVisibleToUser);
        }
    }

    public void setStatus( ArrayList<String> results )
    {
        if(m_voiceStatus != null && results.size() > 0 )
            m_voiceStatus.setText(results.get(0));
    }

    static class CompareByMostChar implements Comparator<ictWordSpotting>
    {
        public int compare(ictWordSpotting c1, ictWordSpotting c2)
        {
            return c2.getCount() - c1.getCount();
        }
    }

    static class CompareByLeastTokens implements Comparator<ictWordSpotting>
    {
        public int compare(ictWordSpotting c1, ictWordSpotting c2)
        {
            return c1.getToken() - c2.getToken();
        }
    }

    public boolean processResults ( ArrayList<String > results)
    {
        Log.i("IAN_", "Got here finally processResults =" + results.size());
        if ( results != null && results.size() > 0 )
        {
            if(results.size() == 1)
                m_voiceStatus.setText(results.get(0));
            else{
                StringBuilder strBuilder = new StringBuilder();
                if( results.size() > 5 )
                    results = (ArrayList<String>)results.subList(0,5);
                for( String result : results)
                    strBuilder.append(result).append("\n");
                m_voiceStatus.setText(strBuilder.toString());
            }

            if( m_Files != null ) {
                for (int i = 0; i < m_Files.size(); i++)
                    m_Files.get(i).Reset();

                for (int i = 0; i < m_Files.size(); i++) {
                        m_Files.get(i).Process(results.get(0));
                }


                ArrayList<ictWordSpotting> withValues = new ArrayList<ictWordSpotting>();
                // Sort by most matches words.
                Collections.sort(m_Files, new CompareByMostChar());

                for (ictWordSpotting word : m_Files) {
                    if (word.getCount() > 0) {
                        withValues.add(word);
                        Log.i("IAN_", word.getOriginalString() + " count = " + word.getCount());
                    }
                }


                if (withValues.size() > 0) {
                    ArrayList<ictWordSpotting> byLowestCount = new ArrayList<ictWordSpotting>();
                    int dwCountHighest = withValues.get(0).getCount();
                    for ( ictWordSpotting word : withValues)
                    {
                        if(word.getCount() != dwCountHighest)
                            break;
                        byLowestCount.add(word);
                    }
                    Collections.sort(byLowestCount, new CompareByLeastTokens());
                    // send message to playlist to play.
                    if (m_RequestPauseListener != null)
                        m_RequestPauseListener.playSong(byLowestCount.get(0).getOriginalString());
                    return (true);
                }
            }
            /*
            StringBuilder strBuilder = new StringBuilder();
            int dwCounter = 0;
            for ( String result : results) {
                WordList wordList = new WordList(result);
                if(m_Files.isIn(wordList.getWords())) {
                    if(dwCounter <= 5 )
                    {
                        strBuilder.append(result).append("\n");
                        dwCounter++;
                    }
                }
            }
            if(dwCounter > 0)
                m_voiceStatus.setText(strBuilder.toString());
            */
        }
        return false;
    }

    public void SetFilesToWatch(List<String> wordsIn )
    {
        m_Files = new ArrayList<ictWordSpotting>();
        for( int i = 0; i < wordsIn.size(); i++)
        {
            ictWordSpotting pWordSpotting = new ictWordSpotting(wordsIn.get(i));
            m_Files.add(pWordSpotting);
        }
    }

    public interface OnRequestPauseListener
    {
        void onRequestPause();
        void onFragmentVisibility ( boolean a_bVisible );
        void onStartListeningRequest();
        void onStopListeningRequest();
        boolean onPermissionCheck();
        void playSong(String a_sFileName );
        boolean IsPlaying();
        void reInitData( VoiceFragment voice );
    }

}
