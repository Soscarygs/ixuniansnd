package com.ixuniansnd.ixuniansnd;

import android.app.Activity;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.os.CountDownTimer;
import android.speech.RecognitionListener;
import android.speech.RecognizerIntent;
import android.speech.SpeechRecognizer;
import android.speech.tts.TextToSpeech;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.widget.MediaController;
import android.widget.Toast;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

import static android.speech.SpeechRecognizer.ERROR_AUDIO;
import static android.speech.SpeechRecognizer.ERROR_CLIENT;
import static android.speech.SpeechRecognizer.ERROR_INSUFFICIENT_PERMISSIONS;
import static android.speech.SpeechRecognizer.ERROR_NETWORK;
import static android.speech.SpeechRecognizer.ERROR_NETWORK_TIMEOUT;
import static android.speech.SpeechRecognizer.ERROR_NO_MATCH;
import static android.speech.SpeechRecognizer.ERROR_RECOGNIZER_BUSY;
import static android.speech.SpeechRecognizer.ERROR_SERVER;
import static android.speech.SpeechRecognizer.ERROR_SPEECH_TIMEOUT;

import com.ixuniansnd.ixuniansnd.ictSpeechActivator;
import com.ixuniansnd.ixuniansnd.SpeechActivationListener;

import com.ixuniansnd.ixuniansnd.ictSpeechModule;
import com.ixuniansnd.ixuniansnd.ictSoundModule;
import com.ixuniansnd.ixuniansnd.ictPreferences;

public class ictSndListenerService extends Service implements ictSpeechModule.OnSpeechResultsListener,
        ictSoundModule.SoundModuleCallback
{

    public static ictSndListenerService s_SndListenerService = null;
    static final int ID = 12345;
    static final int MSG_START_LISTENING = 1;
    static final int MSG_PAUSE_LISTENING = 2;
    static final int MSG_STOP_LISTENING = 3;
    static final int MSG_REGISTER_CLIENT = 4;
    static final int MSG_UNREGISTER_CLIENT = 5;
    static final int MSG_REQUEST_AUDIO_PERMISSION = 6;
    static final int MSG_SEND_SOUND_INTERFACE = 7;
    static final int MSG_POPULATE_MEDIA = 8;
    static final int MSG_SOUND_PREPARE = 9;
    static final int MSG_SHUTDOWN = 10;
    static final int MSG_UNREGISTER_CLIENT_FINAL = 11;
    static final int MSG_UPDATE_PREFERENCES = 12;
    static final int MSG_ON_SONG_COMPLETION = 13;

    static final int MSG_SET_SONG_START = 14;
    static final int MSG_SET_SONG_INDEX = 15;
    static final int MSG_SET_NEXT_TRACK = 16;
    static final int MSG_SET_PREV_TRACK = 17;
    static final int MSG_SET_LOOP_TYPE = 18;
    static final int MSG_DELETE_MEDIA = 19;
    static final int MSG_UNDELETE_MEDIA = 20;


    static final int MSG_SET_VALUE = 100;
    static final int MSG_SET_RESULT = 101;
    static final int MSG_SET_STRING = 102;
    static final String VOICE_RESULT = "VoiceResult";

    ArrayList<Messenger> m_Clients = new ArrayList<Messenger>();
    NotificationManager m_NotificationMan;
    ictSpeechModule m_Activator = null;

    ictSoundModule m_MusicPlayer = null;

    class IncomingHandler extends Handler
    {
        private WeakReference<ictSndListenerService> m_Target;

        IncomingHandler( ictSndListenerService a_Target)
        {
            m_Target = new WeakReference<ictSndListenerService>( a_Target );
        }

        @Override
        public void handleMessage(Message msg)
        {
            ictSndListenerService audioServ = m_Target.get();
            switch(msg.what)
            {
                case MSG_POPULATE_MEDIA:
                    {
                        Bundle bundle = msg.getData();
                        ArrayList<ictFileInfo> files = bundle.getParcelableArrayList("Songs");
                        int songPos = bundle.getInt("SongPosIdx", 0);
                        if ( m_MusicPlayer != null )
                        {
                            m_MusicPlayer.populateFiles(files, songPos);
                            if(files != null && (songPos >= 0 && songPos < files.size())) {
                                setSongIndex(files.get(songPos).getIndex());
                            }
                        }
                    }
                    break;
                case MSG_SOUND_PREPARE:
                    {
                        for(int i = 0; i < m_Clients.size(); i++)
                        {
                            try
                            {
                                Message msgToClient = Message.obtain(null, MSG_SOUND_PREPARE);
                                msgToClient.arg1 = msg.arg1;
                                msgToClient.arg2 = msg.arg2;
                                m_Clients.get(i).send(msgToClient);
                            }catch(RemoteException e)
                            {}
                        }
                    }
                    break;
                case MSG_ON_SONG_COMPLETION:
                    {
                        for(int i = 0; i < m_Clients.size(); i++)
                        {
                            try
                            {
                                Message msgToClient = Message.obtain(null, MSG_ON_SONG_COMPLETION);
                                msgToClient.arg1 = msg.arg1;
                                m_Clients.get(i).send(msgToClient);
                            }catch(RemoteException e)
                            {}
                        }
                    }
                    break;
                case MSG_REGISTER_CLIENT: {
                        m_Clients.add(msg.replyTo);
                        Bundle bundle = msg.getData();
                        SetPreferences((ictPreferences)bundle.getParcelable(ictPreferences.PREF_NAME));
                    }
                    break;
                case MSG_UNREGISTER_CLIENT:
                case MSG_UNREGISTER_CLIENT_FINAL:
                    m_Clients.remove(msg.replyTo);

                    if ( m_Clients.isEmpty())
                    {
                        if(m_MusicPlayer != null) {
                            if(!m_MusicPlayer.isPlaying())
                                stopSelf();
                            else
                            {
                                if(msg.what == MSG_UNREGISTER_CLIENT_FINAL)
                                {
                                    m_MusicPlayer.shutDown();
                                    stopSelf();
                                }
                            }
                        }
                        else
                        {
                            stopSelf();
                        }
                    }
                    break;
                case MSG_SHUTDOWN:
                    stopSelf();
                    break;
                case MSG_UPDATE_PREFERENCES:
                    {
                        Bundle bundle = msg.getData();
                        SetPreferences((ictPreferences)bundle.getParcelable(ictPreferences.PREF_NAME));
                    }
                    break;
                case MSG_START_LISTENING:
                {
                    if(m_Activator == null)
                    {
                        SetSpeechActivator();
                    }
                    else if ( !m_Activator.IsListening())
                    {
                        m_Activator.destroy();
                        SetSpeechActivator();
                    }
                }
                    break;
                case MSG_STOP_LISTENING:
                {
                    if(m_Activator != null)
                    {
                        m_Activator.destroy();
                        m_Activator = null;
                    }
                }
                    break;
                case MSG_SET_VALUE: {
                    int value = msg.arg1;
                    Log.e("IAN_", "Request set value: " + value);
                    for(int i = 0; i < m_Clients.size(); i++) {
                        try {
                            Message msgToClient = Message.obtain(null, MSG_SET_VALUE, value, 0);
                            m_Clients.get(i).send(msgToClient);
                        }catch(RemoteException e)
                        {
                            Log.e("IAN_", "MSG_SET_VALUE Exception");
                        }
                    }
                    break;
                }
                case MSG_REQUEST_AUDIO_PERMISSION:
                {
                    for(int i = 0; i < m_Clients.size(); i++) {
                        try
                        {
                            Message msgToClient = Message.obtain(null, MSG_REQUEST_AUDIO_PERMISSION);
                            m_Clients.get(i).send(msgToClient);
                        }catch(RemoteException e)
                        {}
                    }
                    break;
                }
                case MSG_SET_RESULT:
                {
                    Bundle results = msg.getData();

                    for(int i = 0; i < m_Clients.size(); i++)
                    {
                        try
                        {
                            Message msgToClient = Message.obtain(null, MSG_SET_RESULT);
                            msgToClient.setData(results);
                            m_Clients.get(i).send(msgToClient);
                        }catch(RemoteException e)
                        {

                        }
                    }
                }
                break;
                case MSG_SET_SONG_START:
                    start();
                    break;
                case MSG_SET_SONG_INDEX:
                    setSongIndex(msg.arg1);
                    break;
                case MSG_SET_NEXT_TRACK:
                    playNext(true);
                    break;
                case MSG_SET_PREV_TRACK:
                    playPrev(true);
                    break;
                case MSG_SET_LOOP_TYPE:
                    SetLoopType(msg.arg1);
                    break;
                case MSG_DELETE_MEDIA:
                    DeleteMedia(msg.arg1);
                    break;
                case MSG_UNDELETE_MEDIA:
                    UnDeleteMedia();
                    break;
                default:
                    super.handleMessage(msg);
            }
        }
    }

    final Messenger m_Messenger = new Messenger( new IncomingHandler(this));

    public ictSndListenerService() {
    }

    @Override
    public void onCreate()
    {
        super.onCreate();
        m_NotificationMan = (NotificationManager)getSystemService(NOTIFICATION_SERVICE);
        m_MusicPlayer = new ictSoundModule(this, this);
        m_MusicPlayer.Init();
        ictSndListenerService.s_SndListenerService = this;

        Intent detailsIntent =  new Intent(RecognizerIntent.ACTION_GET_LANGUAGE_DETAILS);

        detailsIntent.setPackage("com.google.android.googlequicksearchbox");

        PackageManager packageManager = getPackageManager();

        for (PackageInfo packageInfo: packageManager.getInstalledPackages(0)) {
            if (packageInfo.packageName.contains("com.google.android.googlequicksearchbox"))
                Log.d("AAA", packageInfo.packageName + ", "  + packageInfo.versionName);
        }
        sendOrderedBroadcast(
                detailsIntent, null, new ictLanguageDetailsChecker(), null, Activity.RESULT_OK, null, null);
    }

    private Notification showNotification()

    {
        CharSequence text = getText(R.string.notification_snd_started);

        //Toast.makeText(this,R.string.notification_snd_started, Toast.LENGTH_SHORT).show();

        PendingIntent contentIntent = PendingIntent.getActivity(this, 0, new Intent(this, MainActivity.class), 0 );

        //Notification notification = new Notification(R.drawable.ic_music_note, text, System.currentTimeMillis());
        //notification.setLatestEventInfo(this, getText(R.string.notification_remote_service), text, contentIntent);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(this);
        Notification notification = builder.setContentIntent(contentIntent).setSmallIcon(R.drawable.ic_music_note).setTicker(text).setWhen(System.currentTimeMillis()).setAutoCancel(true).setContentTitle(getText(R.string.notification_remote_service)).setContentText(text).build();
        return notification;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId)
    {
        super.onStartCommand(intent, flags, startId);
        startForeground(ID, showNotification());
        return START_NOT_STICKY;
    }


    @Override
    public void onDestroy(){
        super.onDestroy();
        if (m_MusicPlayer != null)
        {
            m_MusicPlayer.onDestroy();
        }
        Log.e("IAN_","---> SpeechRecognizer Destroyed");
        m_NotificationMan.cancel(R.string.notification_snd_started);
        Toast.makeText(this,R.string.notification_snd_stopped, Toast.LENGTH_SHORT).show();
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        //throw new UnsupportedOperationException("Not yet implemented");
        return m_Messenger.getBinder();
    }

    public void onResults ( ArrayList<String> results)
    {
        if ( results != null && results.size() > 0 ) {
            try {
                Message message = Message.obtain(null, MSG_SET_RESULT);
                Bundle bundle = new Bundle();
                bundle.putStringArrayList(ictSndListenerService.VOICE_RESULT, results);
                message.setData(bundle);

                m_Messenger.send(message);
            } catch (RemoteException e) {
                Log.e("IAN_", e.toString());
            }
            if(results.size() == 1)
            {
                Log.i("IAN_", "onResults size is 1");
            }
        }
    }

    public void onPermissionRequests()
    {
        try
        {
            Message message = Message.obtain(null, MSG_REQUEST_AUDIO_PERMISSION);
            m_Messenger.send(message);
        }catch(RemoteException e)
        {
            Log.i("IAN_", "onPermissionRequests failed!" + e.toString());
        }
    }

    private void SetSpeechActivator()
    {

        m_Activator = new ictSpeechModule(this, this);
    }

    // MediaController interface
    public int getCurrentPosition()
    {
        return m_MusicPlayer.getCurrentPosition();
    }
    public int getDuration() { return m_MusicPlayer.getDuration(); }
    public boolean isPlaying()
    {
        return m_MusicPlayer.isPlaying();
    }

    public void pause()
    {
        m_MusicPlayer.pause();
    }

    public void seekTo(int pos)
    {
        m_MusicPlayer.seekTo(pos);
    }
    public void start()
    {
        m_MusicPlayer.start();
    }
    public int getAudioSessionId()
    {
        return m_MusicPlayer.getAudioSessionId();
    }
    public boolean hasCurrentTrack() {
        return m_MusicPlayer.hasCurrentTrack();
    }

    public void setSongIndex(int index)
    {
        m_MusicPlayer.setSongIndex(index);
    }

    public void playNext( boolean a_Pressed )
    {
        m_MusicPlayer.playNext( a_Pressed  );
    }
    public void playPrev( boolean a_Pressed )
    {
        m_MusicPlayer.playPrev( a_Pressed );
    }

    List<ictFileInfo> GetMediaList() { return m_MusicPlayer.GetMediaList(); }
    int GetCurrentTrackPos() { return m_MusicPlayer.GetCurrentTrackPos(); }
    int GetLoopType() { return m_MusicPlayer.GetLoopType(); }
    public void SetLoopType ( int a_dwLoopType ) { m_MusicPlayer.SetLoopType( a_dwLoopType );}
    public boolean DeleteMedia( int pos ) { return m_MusicPlayer.DeleteMedia( pos ); }
    public boolean UnDeleteMedia() { return m_MusicPlayer.UnDeleteMedia(); }

    public void onPrepare( int a_dwAudioSessionId, int a_dwSongIndex )
    {
        try
        {
            Message msg = Message.obtain(null, MSG_SOUND_PREPARE);
            msg.arg1 = a_dwAudioSessionId;
            msg.arg2 = a_dwSongIndex;
            m_Messenger.send(msg);
        }catch(RemoteException e)
        {}
    }

    public void onCompletion( int a_dwAudioSessionId )
    {
        try {
            Message msg = Message.obtain(null, MSG_ON_SONG_COMPLETION);
            msg.arg1 = a_dwAudioSessionId;
            m_Messenger.send(msg);
        }catch(RemoteException e)
        {

        }
    }

    public boolean shouldContinuePlaying() {
        if(m_Clients.isEmpty()) {
            m_MusicPlayer.shutDown();
            try
            {
                Message msg = Message.obtain(null, MSG_SHUTDOWN);
                m_Messenger.send(msg);
            }catch(RemoteException e)
            {

            }
            return false;
        }
        else {
            return true;
        }
    }

    protected void SetPreferences( ictPreferences preferences)
    {
        if( preferences != null)
        {
            m_MusicPlayer.setEnableTTS(preferences.m_bTTS);
        }
    }
}

class StreamVolumeData
{
    int m_VolumeVoiceCall = 0;
    int m_VolumeAlarm = 0;
    int m_VolumeStreamRing = 0;
    int m_VolumeStreamSystem = 0;
    int m_VolumeStreamMusic = 0;

    void SaveVolumeValues(AudioManager a_AudioManager) {
        m_VolumeVoiceCall = a_AudioManager.getStreamVolume(AudioManager.STREAM_VOICE_CALL);
        m_VolumeAlarm = a_AudioManager.getStreamVolume(AudioManager.STREAM_ALARM);
        m_VolumeStreamRing = a_AudioManager.getStreamVolume(AudioManager.STREAM_RING);
        m_VolumeStreamSystem = a_AudioManager.getStreamVolume(AudioManager.STREAM_SYSTEM);
        m_VolumeStreamMusic = a_AudioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
        Log.i("IAN_", "VolumeVoiceCall: " + m_VolumeVoiceCall);
        Log.i("IAN_", "VolumeAlarm: " + m_VolumeAlarm);
        Log.i("IAN_", "VolumeStreamRing: " + m_VolumeStreamRing);
        Log.i("IAN_", "VolumeStreamMusic: " + m_VolumeStreamMusic);
        Log.i("IAN_", "VolumeStreamSystem: " + m_VolumeStreamSystem);
    }
}

class ictLanguageDetailsChecker extends BroadcastReceiver
{
    private List<String> supportedLanguages;

    private String languagePreference;


    @Override
    public void onReceive(Context context, Intent intent)
    {
        Bundle results = getResultExtras(true);
        if (results.containsKey(RecognizerIntent.EXTRA_LANGUAGE_PREFERENCE))
        {
            languagePreference =
                    results.getString(RecognizerIntent.EXTRA_LANGUAGE_PREFERENCE);
        }
        if (results.containsKey(RecognizerIntent.EXTRA_SUPPORTED_LANGUAGES))
        {
            supportedLanguages =
                    results.getStringArrayList(
                            RecognizerIntent.EXTRA_SUPPORTED_LANGUAGES);
            for(int i = 0; i < supportedLanguages.size(); i++) {
                Log.d("IAN_", supportedLanguages.get(i));
            }
        }
    }
}

