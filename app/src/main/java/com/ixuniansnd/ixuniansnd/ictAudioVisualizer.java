package com.ixuniansnd.ixuniansnd;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.text.TextPaint;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.media.audiofx.Visualizer;
import android.graphics.Color;

import java.util.ArrayList;
import java.util.Random;

import static java.lang.Math.abs;

/**
 * TODO: document your custom view class.
 */
public class ictAudioVisualizer extends View {
    private String mExampleString; // TODO: use a default from R.string...
    private int mExampleColor = Color.RED; // TODO: use a default from R.color...
    private float mExampleDimension = 0; // TODO: use a default from R.dimen...
    private Drawable mExampleDrawable;

    private TextPaint mTextPaint;
    private float mTextWidth;
    private float mTextHeight;

    private byte[] mBytes;
    private float[] mPoints;
    private Rect mRect = new Rect();
    private Paint mForePaint = new Paint();
    private static final int c_dwNumPoints = 20;

    ArrayList<ictAudioRendererMethod> m_RenderMethod = new ArrayList<ictAudioRendererMethod>();

    public ictAudioVisualizer(Context context) {
        super(context);
        init(null, 0);
    }

    public ictAudioVisualizer(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs, 0);
    }

    public ictAudioVisualizer(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(attrs, defStyle);
    }

    private void init(AttributeSet attrs, int defStyle) {
        // Load attributes
        final TypedArray a = getContext().obtainStyledAttributes(
                attrs, R.styleable.ictAudioVisualizer, defStyle, 0);

        mExampleString = a.getString(
                R.styleable.ictAudioVisualizer_exampleString);
        mExampleColor = a.getColor(
                R.styleable.ictAudioVisualizer_exampleColor,
                mExampleColor);
        // Use getDimensionPixelSize or getDimensionPixelOffset when dealing with
        // values that should fall on pixel boundaries.
        mExampleDimension = a.getDimension(
                R.styleable.ictAudioVisualizer_exampleDimension,
                mExampleDimension);

        if (a.hasValue(R.styleable.ictAudioVisualizer_exampleDrawable)) {
            mExampleDrawable = a.getDrawable(
                    R.styleable.ictAudioVisualizer_exampleDrawable);
            mExampleDrawable.setCallback(this);
        }

        a.recycle();

        // Set up a default TextPaint object
        mTextPaint = new TextPaint();
        mTextPaint.setFlags(Paint.ANTI_ALIAS_FLAG);
        mTextPaint.setTextAlign(Paint.Align.LEFT);

        mBytes = null;
        mForePaint.setStrokeWidth(1f);
        mForePaint.setAntiAlias(true);
        mForePaint.setColor(Color.rgb(255, 255, 255));

        m_RenderMethod.add( new ictAudioRendererBar());

        // Update TextPaint and text measurements from attributes
        invalidateTextPaintAndMeasurements();
    }

    private void invalidateTextPaintAndMeasurements() {
        mTextPaint.setTextSize(mExampleDimension);
        mTextPaint.setColor(mExampleColor);
        mTextWidth = mTextPaint.measureText(mExampleString);

        Paint.FontMetrics fontMetrics = mTextPaint.getFontMetrics();
        mTextHeight = fontMetrics.bottom;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);


        // TODO: consider storing these as member variables to reduce
        // allocations per draw cycle.
        int paddingLeft = getPaddingLeft();
        int paddingTop = getPaddingTop();
        int paddingRight = getPaddingRight();
        int paddingBottom = getPaddingBottom();

        int contentWidth = getWidth() - paddingLeft - paddingRight;
        int contentHeight = getHeight() - paddingTop - paddingBottom;

        if (mBytes == null) {
            return;
        }
        if (mPoints == null || mPoints.length < mBytes.length * 4) {
            mPoints = new float[mBytes.length * 4];
        }
        mRect.set(paddingLeft, paddingTop, contentWidth+paddingRight, contentHeight+paddingBottom);
        canvas.drawRect(mRect, mForePaint);
        //mRect.set(0,0,getWidth(), getHeight());
        m_RenderMethod.get(0).onRender( canvas, mRect, mBytes, mPoints);

    }

    /**
     * Gets the example string attribute value.
     *
     * @return The example string attribute value.
     */
    public String getExampleString() {
        return mExampleString;
    }

    /**
     * Sets the view's example string attribute value. In the example view, this string
     * is the text to draw.
     *
     * @param exampleString The example string attribute value to use.
     */
    public void setExampleString(String exampleString) {
        mExampleString = exampleString;
        invalidateTextPaintAndMeasurements();
    }

    /**
     * Gets the example color attribute value.
     *
     * @return The example color attribute value.
     */
    public int getExampleColor() {
        return mExampleColor;
    }

    /**
     * Sets the view's example color attribute value. In the example view, this color
     * is the font color.
     *
     * @param exampleColor The example color attribute value to use.
     */
    public void setExampleColor(int exampleColor) {
        mExampleColor = exampleColor;
        invalidateTextPaintAndMeasurements();
    }

    /**
     * Gets the example dimension attribute value.
     *
     * @return The example dimension attribute value.
     */
    public float getExampleDimension() {
        return mExampleDimension;
    }

    /**
     * Sets the view's example dimension attribute value. In the example view, this dimension
     * is the font size.
     *
     * @param exampleDimension The example dimension attribute value to use.
     */
    public void setExampleDimension(float exampleDimension) {
        mExampleDimension = exampleDimension;
        invalidateTextPaintAndMeasurements();
    }

    /**
     * Gets the example drawable attribute value.
     *
     * @return The example drawable attribute value.
     */
    public Drawable getExampleDrawable() {
        return mExampleDrawable;
    }

    /**
     * Sets the view's example drawable attribute value. In the example view, this drawable is
     * drawn above the text.
     *
     * @param exampleDrawable The example drawable attribute value to use.
     */
    public void setExampleDrawable(Drawable exampleDrawable) {
        mExampleDrawable = exampleDrawable;
    }
    public void setData ( byte [] data ) {
        mBytes = data;
        invalidate();
    }
}


abstract class ictAudioRendererMethod
{
    public abstract void onRender(Canvas canvas, Rect mRect, byte [] mBytes, float [] mPoints );
}

class ictAudioRendererBar extends ictAudioRendererMethod
{
    static final int c_dwNumPoints = 10;

    static final int c_dwNumBarColors = 10;
    private Paint [] m_BarColors = new Paint[c_dwNumBarColors];

    ictAudioRendererBar()
    {
        float fDelta = 360.f/c_dwNumBarColors;
        float [] hsv = new float[3];
        hsv[0] = 0.f;
        hsv[1] = 1.f;
        hsv[2] = 1.f;
        for( int i = 0; i < c_dwNumBarColors; i++ )
        {
            //hsv[0] += fDelta;
            hsv[0] = 30.f;
            int argb = Color.HSVToColor(hsv);
            m_BarColors[i] = new Paint();
            m_BarColors[i].setColor(Color.rgb(Color.red(argb), Color.green(argb), Color.blue(argb)));
            m_BarColors[i].setStrokeWidth(1f);
            m_BarColors[i].setAntiAlias(true);
        }
    }

    public void onRender(Canvas canvas, Rect mRect, byte [] mBytes, float [] mPoints )
    {
        if(mBytes.length> 1) {

            int grid = mRect.height() / 2 / c_dwNumBarColors;

            int dwLenToUse = mBytes.length < c_dwNumPoints ? mBytes.length : c_dwNumPoints;

            int avg = 0;
            int max = mBytes[0];
            int min = mBytes[0];
            for (int i = 0; i < dwLenToUse; i++) {
                avg += mBytes[i];
                if (mBytes[i] < min)
                    min = mBytes[i];
                if (mBytes[i] > max)
                    max = mBytes[i];
            }
            avg /= dwLenToUse;
            int range = abs(max - min);


            for (int i = 0; i < dwLenToUse - 1; i++) {
                float fDelta = (int) mBytes[i] - avg;
                float fDeltaOverRange = fDelta / range;
                int heightBar = (int) (fDeltaOverRange * mRect.height() / 2);
                int top = 0, bottom = 0, center = mRect.bottom - mRect.height() / 2;
                if (heightBar >= 0) {
                    top = center - heightBar;
                    bottom = center;
                } else {
                    top = center;
                    bottom = center - heightBar;
                }

                mPoints[i * 4] = mRect.width() * i / (dwLenToUse - 1) + mRect.left;
                //mPoints[i * 4 + 1] = mRect.bottom - ((mBytes[i] + 128.f)/256.f)*mRect.height()/2;
                mPoints[i * 4 + 1] = top;

                mPoints[i * 4 + 2] = mRect.width() * (i + 1) / (dwLenToUse - 1) + mRect.left;
                //mPoints[i * 4 + 3] = mRect.bottom;
                mPoints[i * 4 + 3] = bottom;

                Paint paintCur = m_BarColors[c_dwNumBarColors - 1];
                int curGrid = abs(heightBar) / grid;
                if (curGrid < m_BarColors.length)
                    paintCur = m_BarColors[curGrid];

                canvas.drawRect(mPoints[i * 4], mPoints[i * 4 + 1], mPoints[i * 4 + 2], mPoints[i * 4 + 3], paintCur);
            }
        }
    }
}

// AUDIO Producer.
class ictAudioVisualizerProducer implements Visualizer.OnDataCaptureListener
{

    public interface onDataReceivedInterface {
        void onDataReceived(byte[] data);
    }

    private Visualizer m_Visualizer = null;
    private byte [] m_Bytes = null;
    private byte [] m_FFTBytes = null;

    onDataReceivedInterface m_Owner = null;

    public ictAudioVisualizerProducer( onDataReceivedInterface a_Owner )
    {
        try
        {
            m_Owner = a_Owner;

        }catch(ClassCastException e)
        {

        }

    }

    void Reset( int a_dwSessionId ) {
        onRelease();
        if (m_Visualizer == null) {
            try {
                m_Visualizer = new Visualizer(a_dwSessionId);
            } catch (RuntimeException e) {
                Log.e("IANE_", e.toString());
                m_Visualizer = null;
            }
        }
        if (m_Visualizer != null) {
            try {
                int[] captureSize = Visualizer.getCaptureSizeRange();
                Log.i("IANV_", "capture min:" + captureSize[0] + " max:" + captureSize[1] +
                        " maxRate:" + Visualizer.getMaxCaptureRate());
                m_Visualizer.setCaptureSize(Visualizer.getCaptureSizeRange()[0]);
                m_Visualizer.setDataCaptureListener(this, Visualizer.getMaxCaptureRate() / 2, false, true);
            }catch(IllegalStateException e)
            {
                Log.e("IANE_", e.toString());
            }
        }
    }

    void SetEnable ( boolean a_bEnable )
    {
        if(m_Visualizer != null)
        {
            m_Visualizer.setEnabled( a_bEnable );
        }
    }

    public void onRelease()
    {
        if ( m_Visualizer != null) {
            try {
                m_Visualizer.release();
                m_Visualizer = null;
            }catch(Exception e)
            {
                Log.i("IAN_","Is this causing hanged?");
            }
        }
    }

    public void onWaveFormDataCapture( Visualizer vis, byte [] bytes, int samplingRate)
    {
        UpdateWaveFormData( bytes );
        /*
        if ( bytes.length > 0 ) {
            byte min = bytes[0];
            byte max = bytes[0];
            int avg = bytes[0];
            for (int i = 1; i < bytes.length; i++) {
                avg += bytes[i];
                if(bytes[i] < min)
                    min = bytes[i];
                if(bytes[i] > max)
                    max = bytes[i];
            }
            Log.i("IAN3_","min=" + min + " max=" + max + " bytes=" + bytes.length + " avg =" + avg/bytes.length);
        }
        */
    }
    public void onFftDataCapture( Visualizer vis, byte [] bytes, int a_dwSamplingRate)
    {
        UpdateFFTData ( bytes );
    }

    protected void UpdateWaveFormData( byte [] bytes )
    {
        m_Bytes = bytes;
        if(m_Owner != null)
            m_Owner.onDataReceived( bytes );
    }

    protected void UpdateFFTData ( byte [] bytes )
    {
        m_FFTBytes = bytes;
        if(m_Owner != null)
        {
            m_Owner.onDataReceived(bytes);
        }
    }
}