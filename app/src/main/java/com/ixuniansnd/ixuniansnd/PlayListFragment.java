package com.ixuniansnd.ixuniansnd;

import android.app.Activity;
import android.content.Context;
import android.content.ContextWrapper;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.MediaController;
import android.speech.tts.TextToSpeech;
import com.ixuniansnd.ixuniansnd.ictAudioVisualizerProducer;


import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import com.ixuniansnd.ixuniansnd.ictSoundModule;

/**
 * A fragment representing a list of Items.
 * <p/>
 * Activities containing this fragment MUST implement the {@link OnListFragmentInteractionListener}
 * interface.
 */
import android.widget.MediaController.MediaPlayerControl;
import android.widget.RelativeLayout;

import org.w3c.dom.Text;

import static android.R.attr.fragment;
import com.ixuniansnd.ixuniansnd.ictMediaFragmentBase;


public class PlayListFragment extends ictMediaFragmentBase
{
    public static final String PLAYLIST_KEY = "playlist";
    private volatile List<ictFileInfo> m_AudioInfo = null;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public PlayListFragment() {
        super();
    }

    // TODO: Customize parameter initialization
    @SuppressWarnings("unused")
    public static PlayListFragment newInstance(int columnCount, List<ictFileInfo> a_AudioInfo ) {

        ArrayList<ictFileInfo> audioArrayList = new ArrayList<ictFileInfo>(a_AudioInfo);
        PlayListFragment fragment = new PlayListFragment();
        Bundle args = new Bundle();
        args.putParcelableArrayList(PlayListFragment.PLAYLIST_KEY, audioArrayList);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        m_AudioInfo = null;
        if (getArguments() != null) {
            m_AudioInfo = getArguments().getParcelableArrayList(PlayListFragment.PLAYLIST_KEY);

        }

    }

    public void populateRecylerAdapter(List<ictFileInfo> files)
    {
        View view = getView();
        if(view != null) {
            RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.list);
            if (recyclerView != null) {
                RecyclerView.Adapter adapter = recyclerView.getAdapter();
                if (adapter instanceof MyPlayListRecyclerViewAdapter) {
                    MyPlayListRecyclerViewAdapter recyclerAdapter = (MyPlayListRecyclerViewAdapter) adapter;
                    recyclerAdapter.reInit(files);
                }
            }
        }
    }

    public void reInit( int a_dwColumnCount, List<ictFileInfo> a_AudioInfo )
    {
        m_AudioInfo = a_AudioInfo;
        populateRecylerAdapter(m_AudioInfo);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_playlist_list, container, false);
        RecyclerView recyclerView = (RecyclerView)view.findViewById(R.id.list);

        // Set the adapter
        if (recyclerView != null) {
            Context context = recyclerView.getContext();

            recyclerView.setLayoutManager(new LinearLayoutManager(context));

            recyclerView.setAdapter(new MyPlayListRecyclerViewAdapter(MyPlayListRecyclerViewAdapter.MyPlaylistType.ICT_SONG_LIST, getActivity(), m_AudioInfo, mListener));
            recyclerView.setNestedScrollingEnabled(false);
        }
        return view;
    }

    @Override
    public void onViewStateRestored(Bundle savedInstanceState)
    {
        super.onViewStateRestored(savedInstanceState);

    }

    @Override
    public void onStart()
    {
        View view = getView();
        RecyclerView recyclerView = (RecyclerView)view.findViewById(R.id.list);
        setController(view);

        recyclerView.setOnTouchListener( new View.OnTouchListener()
        {
            public boolean onTouch(View v, MotionEvent event)
            {
                if(event.getAction() == MotionEvent.ACTION_MOVE)
                {
                    onPlaylistTouch();
                }
                return false;
            }
        });

        super.onStart();
    }

    private void setTrackIndex( int a_dwCurrentTrack)
    {
        if(a_dwCurrentTrack < m_AudioInfo.size()){
            setSong(m_AudioInfo.get(a_dwCurrentTrack));
        }
    }

    @Override
    public void onResume()
    {
        super.onResume();
    }
    public void setSong(ictFileInfo a_FileInfo)
    {
        setSongIndex(a_FileInfo.getIndex());
    }
}
