package com.ixuniansnd.ixuniansnd;

import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;

/**
 * Created by user2 on 6/24/2017.
 */

public class ictItemTouchHelperCallback extends ItemTouchHelper.Callback  {
    public interface ictItemTouchHelperAdapter
    {
        boolean onItemMove(int a_dwFromPos, int a_dwToPos );
        void onItemDismiss( int a_dwPos );
    }

    private final ictItemTouchHelperAdapter m_Adapter;

    public ictItemTouchHelperCallback(ictItemTouchHelperAdapter a_Adapter )
    {
        m_Adapter = a_Adapter;
    }

    @Override
    public boolean isLongPressDragEnabled() { return true; }

    @Override
    public boolean isItemViewSwipeEnabled() { return true; }

    @Override
    public int getMovementFlags(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder)
    {
        int dragFlags = ItemTouchHelper.UP | ItemTouchHelper.DOWN;
        int swipeFlags = ItemTouchHelper.START | ItemTouchHelper.END;
        return makeMovementFlags(dragFlags, swipeFlags);
    }

    @Override
    public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target)
    {
        m_Adapter.onItemMove(viewHolder.getAdapterPosition(), target.getAdapterPosition());
        return true;
    }

    @Override
    public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction)
    {
        m_Adapter.onItemDismiss(viewHolder.getAdapterPosition());
    }
}
