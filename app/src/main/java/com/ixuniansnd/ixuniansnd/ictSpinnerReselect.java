package com.ixuniansnd.ixuniansnd;

import android.content.Context;
import android.util.AttributeSet;
import android.support.v7.widget.AppCompatSpinner;

/**
 * Created by user2 on 6/22/2017.
 */

public class ictSpinnerReselect extends AppCompatSpinner {
    OnItemSelectedListener m_Listener = null;
    public ictSpinnerReselect(Context a_Context, AttributeSet a_AttribSet)
    {
        super(a_Context, a_AttribSet);
    }

    @Override
    public void setSelection(int position) {
        super.setSelection(position);
        if (m_Listener != null)
            m_Listener.onItemSelected(this, getSelectedView(), position, getSelectedItemId());

    }


    public void setOnItemSelectedEvenIfUnchangedListener( OnItemSelectedListener a_Listener )
    {
        m_Listener = a_Listener;
    }
}
