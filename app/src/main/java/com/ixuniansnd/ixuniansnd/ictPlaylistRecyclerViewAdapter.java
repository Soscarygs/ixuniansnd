package com.ixuniansnd.ixuniansnd;

import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;
import com.ixuniansnd.ixuniansnd.ictItemTouchHelperCallback;

import static com.ixuniansnd.ixuniansnd.MyPlayListRecyclerViewAdapter.MyPlaylistType.ICT_PLAYLIST;
import static com.ixuniansnd.ixuniansnd.MyPlayListRecyclerViewAdapter.MyPlaylistType.ICT_SONG_LIST;

/**
 * Created by user2 on 6/19/2017.
 */

public class ictPlaylistRecyclerViewAdapter extends RecyclerView.Adapter<ictPlaylistRecyclerViewAdapter.ViewHolder> {
    List<String> m_playlists = null;
    ictPlayListView.OnGetMediaInfoInterface m_MediaInfo = null;
    public ictPlaylistRecyclerViewAdapter(List<String> a_PlayLists, ictPlayListView.OnGetMediaInfoInterface a_MediaInfo )
    {
        m_playlists = a_PlayLists;
        m_MediaInfo = a_MediaInfo;
    }

    @Override
    public ictPlaylistRecyclerViewAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_playlist_list_playlist, parent, false);
        ictPlaylistRecyclerViewAdapter.ViewHolder pViewHolder =  new ictPlaylistRecyclerViewAdapter.ViewHolder(view);
        return pViewHolder;
    }

    public void reInit(List<String> a_Playlists )
    {
        m_playlists = a_Playlists;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        if( m_playlists == null)
            return 0;
        return m_playlists.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnCreateContextMenuListener {
        public View mView;
        public final ImageView mIdView;
        public final TextView mContentView;
        public final ImageView m_MenuContext;


        public ViewHolder(View view) {
            super(view);
            mView = view;
            mIdView = (ImageView) view.findViewById(R.id.media_type_playlist_icon);
            mIdView.setImageResource(R.drawable.ic_list_black_24dp);
            mContentView = (TextView) view.findViewById(R.id.playlist_list_playlist_title);
            m_MenuContext = (ImageView)view.findViewById(R.id.playlist_playlist_ctx_img);
            m_MenuContext.setImageResource(R.drawable.ic_more_vert_black_24dp);
            m_MenuContext.setOnCreateContextMenuListener(this);
        }

        private final MenuItem.OnMenuItemClickListener onPlayListContextMenu = new MenuItem.OnMenuItemClickListener() {

            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.playlist_playlist_ctx_img: {

                            AlertDialog.Builder confirmDelete = new AlertDialog.Builder( m_MediaInfo.GetContext());
                            confirmDelete.setTitle(R.string.delete_playlist);
                            String msg = m_MediaInfo.GetContext().getString(R.string.delete_playlist) + " : ";
                            int posCursor = getAdapterPosition();
                            if(posCursor != -1)
                            {
                                msg += m_playlists.get(posCursor) + " ?";
                            }
                            confirmDelete.setMessage(msg);
                            confirmDelete.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener()
                            {
                                public void onClick(DialogInterface dialog, int id)
                                {

                                }
                            });
                            confirmDelete.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    int posCursor = getAdapterPosition();
                                    if (posCursor != -1) {
                                        String songName = m_playlists.get(posCursor);
                                        if (m_MediaInfo.DeletePlaylist(songName)) {
                                            m_playlists.remove(posCursor);
                                            notifyItemRemoved(posCursor);
                                            notifyItemRangeChanged(posCursor, m_playlists.size());
                                        }
                                    }
                                }
                            });
                            Dialog dialog = confirmDelete.create();
                            dialog.show();
                    }
                    break;
                }
                return true;
            }
        };

        @Override
        public void onCreateContextMenu(ContextMenu menu, View view, ContextMenu.ContextMenuInfo info)
        {
            MenuItem playListItem = menu.add(Menu.NONE, R.id.playlist_playlist_ctx_img, Menu.NONE, R.string.delete_playlist);
            playListItem.setOnMenuItemClickListener(onPlayListContextMenu);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + mContentView.getText() + "'";
        }
    }

    static final int m_UnselColor = Color.rgb(0xe6, 0xe6, 0xe6);
    @Override
    public void onBindViewHolder(final ictPlaylistRecyclerViewAdapter.ViewHolder holder, final int position) {
        holder.mContentView.setText(m_playlists.get(position));
        String playlistName = m_MediaInfo.GetSelectedPlaylist();
        if(playlistName != null) {
            boolean bSelected = playlistName.equalsIgnoreCase(m_playlists.get(position));
            holder.mView.setBackgroundColor(bSelected ? Color.CYAN : m_UnselColor);
        }
        else
        {
            holder.mView.setBackgroundColor(m_UnselColor);
        }
        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (null != m_MediaInfo) {
                    int pos = position;

                    // Notify the active callbacks interface (the activity, if the
                    // fragment is attached to one) that an item has been selected.
                    m_MediaInfo.SelectPlaylist(m_playlists.get(pos));
                    notifyDataSetChanged();
                }

            }
        });

    }
}
